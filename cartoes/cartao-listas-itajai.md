# Animação - Fatos Curiosos


## Parte da frente do cartão 

> usar identidade visual de cartão Scratch 

* Apresente fatos curiosos sobre uma cidade

`mostrar a imagem da animação do jogo finalizado - fatoscuriosos-animacao`

Fatos Curiosos          (1)             Scratch 

## Parte de trás do cartão 

> usar identidade visual de cartão Scratch 

Pense Fatos Curiosos

https://scratch.mit.edu/projects/313091232

## Prepare-se

* Escolha uma cidade
* Pesquise 6 fatos curiosos sobre essa cidade na internet
* Procure uma imagem da cidade na internet
* Escolha essa imagem como cenário do palco `mostrar imagem Carregar Cenário - fatoscuriosos-cenario`
* Clique no ator padrão 
* Crie uma lista chamada **fatos** `mostrar imagem da seção de Variáveis - fatoscuriosos-variaveis`
* Crie uma variável chamada **posicao** 

## Adicione este código 

> usar a imagem `fatoscuriosos-codigo` para substituir o bloco abaixo

* Substitua os fatos de 1 a 6 dos blocos adicione pelos fatos curiosos encontrados.

```
    defina o bloco **fatos sobre itajaí** 
        apague todos os itens da lista 
        adicione o fato 1 a lista
        adicione o fato 2 a lista
        adicione o fato 3 a lista
        adicione o fato 4 a lista
        adicione o fato 5 a lista
        adicione o fato 6 a lista

    defina o bloco **pense cada fato**
        mude o valor da variável posicao para 1
        repita os blocos abaixo uma vez para cada fato
            pense o fato da posicao por **3** segundos
            adicione 1 para variável posicao

    quando o ícone da bandeira verde for clicado
        chame bloco **fatos sobre itajaí**
        chame bloco **pense cada fato** 
```


## Teste sua animação

Clique na `colocar aqui o ícone da bandeira verde` para iniciar

## Blocos Utilizados

> colocar uma imagem para cada bloco. Se faltar espaço os blocos podem ser colocados um do lado do outro. As imagens podem ser recortadas do seguinte editor <https://scratch.mit.edu/projects/313091232/editor/>

adicione *valor* a *lista* 

apague todos os itens de *lista* 

chame meu bloco

defina *bloco* 

mude *variável* para *valor* 

pense *valor* por *número* segundos

quando o ícone da bandeira verde for clicado

repita *número* vezes