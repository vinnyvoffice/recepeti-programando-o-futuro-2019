## Aula 02

### Unidade 
- 1

### Nome
- Explorando o ambiente do Scratch

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá oferecido feedback em grupo sobre projetos da turma
    - irá criar um projeto com a restrição de apenas poder usar 10 blocos
    - irá investigar as possibilidfades criativas com o Scratch, explorando alguns dos milhões de projetos no site 
    - irá organizar uma coleção de 3 ou mais projetos Scratch em um estúdio Scratch 
### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Leia os roteiros da aula
- Leia as questões da seção Revisão para lembrar os resultados esperados

#### Contextualizando (20 minutos)
- Apresente as 3 atividades previstas: estúdio, 10 blocos e grupo de críticas
- Estime os participantes lendo as questões das seção Reflexão
- Demonstre como criar um novo estúdio usando o roteiro 
- Mostre exemplos de estúdios da comunidade e dos educadores
- Oriente os alunos sobre a atividade 10 blocos usando o respectivo roteiro
- Apresente aos alunos a ideia de um grupo de críticas: um pequeno grupo que compartilha ideias e projetos em andamento uns dos outros, a fim de obter feedback e sugestões para melhorar o desenvolvimentos dos projetos.
- Apresente o roteiro Trabalhando em Grupo para orientar os alunos a dar feedback.
- Defina os grupos de 4 pessoas. 
- Oriente os participantes sobre a gestão do tempo para obter os resultados

#### Praticando (30 minutos)
- Oriente para que cada participante crie seu estúdio de inspirações
- Dê aos participantes 10 minutos para procurarem projetos interessantes e adicionarem ao estúdio
- Oriente para que cada participante registre suas estratégias de exploração na descrição do estúdio
- Oriente os grupos a se juntarem
- Oriente os grupos a criar um novo projeto para a atividade dos 10 blocos
- Oriente os grupos a usar pelo menos uma vez cada um dos 10 blocos no projeto
- Oriente os grupos a escolher 3 para discutir conforme o roteiro Trabalhando em Grupo
- Oriente cada membro do grupo a compartilhar o feedback de um dos projetos discutidos como comentário no Scratch

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula com o nome de cada membro na descrição
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem os projetos com 10 Blocos de seus grupos
- Peça que 3 voluntários apresentem seus estúdios de inspiração

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Roteiro: Trabalhando em Grupo
- Roteiro: 10 Blocos 
- Estúdio: 10 Blocos
    - http://scratch.mit.edu/studios/5282810
    - https://scratch.mit.edu/studios/475480/
- Roteiro: Meu Estúdio 
- Exemplos de estúdios
    - http://scratch.mit.edu/studios/5288355 
    - http://scratch.mit.edu/studios/5288375
    - http://scratch.mit.edu/studios/5288388 
    - http://scratch.mit.edu/studios/5288445    
    - Estúdios do Programando o Futuro
        - http://scratch.mit.edu/studios/999999/

#### Complementares
- Atividade: 10 Blocos do guia curricular de Computação Criativa
- Atividade: Meu Estúdio do guia curricular de Computação Criativa
- Atividade: Trabalhando em Grupo do guia curricular de Computação Criativa
- Atividade: Lista de Jogos do guia curricular de Computação Criativa
- Atividade: Jogos Básicos do guia curricular de Computação Criativa
### Reflexão
+  VERMELHO: Há algo que não está funcionando bem ou que pode ser melhorado?
+  AMARELO: Há algo que está confuso ou que pode ser feito de forma diferente?
+  VERDE: O que funciona bem, ou que você realmente gosta no projeto?
+  O que foi difícil ao usar apenas 10 blocos?
+  O que foi fácil ao usar apenas 10 blocos?
+  Como isso fez você pensar nas coisas de forma diferente?
+ Quais estratégias de busca você usou para encontrar projetos interessantes?
+  Como cada projeto usado como exemplo pode ajudar com o trabalho futuro?
+  É importante dar crédito às fontes de inspiração. Como você pode dar crédito pela inspiração desses projetos?

### Revisão
- Todos os alunos tiveram a chance de compartilhar seu o feedback do grupo?
- Os feedbacks estão de acordo com as diretrizes da comunidade Scratch?
+ Os projetos incluem todos os 10 blocos?
+  Como diferentes alunos reagem à ideia de criar com
restrições? O que isso pode lhe dizer sobre como esse aluno aprende?
+ Existem três ou mais projetos no estúdio?
+  O que esses projetos informam sobre os interesses de
seus alunos?

### Dicas
- Favoreça o compartilhamento dos comentários de grupo de crítica aos comentários individuais em futuras aulas
- Explore os projetos no estúdio 10 Blocos para inspirar os participantes
- Compartilhe o arquivo digital dos roteiros da aula para facilitar a consulta

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
