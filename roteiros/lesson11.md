## Aula 11

### Unidade 
- 4

### Nome
- Jogo de Pega-Pega

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá criado mais um jogo de pega-pega dessa vez utilizando setas direcionais para movimentar o ator

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem seus jogos

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/chase
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/ChaseGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/chase-cards.pdf

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: cards-chase-card1 Mova para direita e Para esquerda
    - Cartão 2 resolvido: cards-chase-card2 Mova para cima e para baixo
    - Cartão 3 resolvido: cards-chase-card3 Pegue a estrela
    - Cartão 4 resolvido: cards-chase-card4 Reproduza um som
    - Cartão 5 resolvido: cards-chase-card5 Marque pontos
    - Cartão 6 resolvido: cards-chase-card6 Crie novos níveis
    - Cartão 7 resolvido: cards-chase-card7 Exiba uma mensagem de vitória
- Projetos do Estúdio Jogo de Pega-Pega 
    - https://scratch.mit.edu/studios/25032662/

### Reflexão
- Quando o jogo termina?
- O que você achou mais difícil de fazer? 
- O que você achou foi mais fácil de fazer?
- O que você gostou no seu jogo? 
- O que você não gostou no seu jogo?
- O que você gostou em outros jogos que você viu no estúdio da aula?
- Quais blocos você já usou nas aulas anteriores?
- Quais blocos você ainda não havia  usado nas aulas anteriores?

### Revisão
- Todos os participantes conseguiram criar seus jogos?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
