## Aula 03

### Unidade 
- 2

### Nome
- Criando uma História

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - Terá criado uma história com cenários, personagens e diálogos

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem suas histórias

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/story
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/StoryGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/story-cards.pdf

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: cards-story-card1 https://scratch.mit.edu/projects/316427682/ Comece uma história  
    - Cartão 2 resolvido: cards-story-card2 https://scratch.mit.edu/projects/316428073/ Crie um diálogo
    - Cartão 3 resolvido: cards-story-card3 https://scratch.mit.edu/projects/316428430/ Troque o cenário
    - Cartão 4 resolvido: cards-story-card4 https://scratch.mit.edu/projects/316428884/ Clique em um ator
    - Cartão 5 resolvido: cards-story-card5 https://scratch.mit.edu/projects/316428925/ Adicione sua voz 
    - Cartão 6 resolvido: cards-story-card6 https://scratch.mit.edu/projects/316429309/ Deslize para algum lugar
    - Cartão 7 resolvido: cards-story-card7 https://scratch.mit.edu/projects/316429500/ Entre no palco
    - Cartão 8 resolvido: cards-story-card8 https://scratch.mit.edu/projects/316429808/ Responda a um ator
    - Cartão 9 resolvido: cards-story-card9 https://scratch.mit.edu/projects/316430002/ Adicione uma cena 
- Projetos do Estúdio Crie uma História
    - https://scratch.mit.edu/studios/25032641/
    - https://scratch.mit.edu/studios/3757922/

### Reflexão
- Existem blocos que só podem ser utilizados em cenários? Cite 2.
- Existem blocos que só podem ser utilizados em atores? Cite 2.
- Quais são as duas formas de sincronizar as falas dos personagens em um diálogo? Qual a diferença entre elas?

### Revisão
- Todos os participantes conseguiram criar suas histórias?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
