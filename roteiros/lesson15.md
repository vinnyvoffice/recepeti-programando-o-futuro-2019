## Aula 15

### Unidade 
- 5

### Nome
- Desenvolvendo Meu Projeto Final 

### Tempo
- 90 a 180 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - utilizará conceitos e práticas computacionais para desenvolver o Scratch do seu projeto
    - trabalhará em pequenos grupos para dar um feedback preliminar sobre os projetos

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista aos vídeos 
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os alunos lendo as questões das seção Reflexão
- Apresente o roteiro Definindo Projetos, dando aos alunos tempo para debater e responder. E oriente para que apresentem suas ideias em 1 minuto.
- Apresente os roteiros Planejamento de Projeto e Esboços de Projeto para os participantes. Dê tempo para que respondam.
- Peça para que 3 voluntários apresentem suas respostas à turma 

#### Praticando (30 minutos)
- Dê aos alunos tempo para trabalhar em seus projetos conforme planejamento e esboços
- Caso a aula tenha 180 minutos, faça 3 sessões de 30 minutos com intervalos de 10 minutos para descanso e replanejamento

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Apresente o roteiro Feedback do Projeto e oriente os participantes a respondê-lo na descrição em seu próprio projeto
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários comentem a situação de seus projetos finais

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Roteiro: Definindo Projetos
- Roteiro: Planejamento de Projeto
- Roteiro: Esboços do Projeto
- Roteiro: Feedback do Projeto

#### Complementares
- Atividade: Definindo projetos do guia curricular de Computação Criativa
- Atividade: Planejando do guia curricular de Computação Criativa
- Atividade: Trabalhando no projeto do guia curricular de Computação Criativa
- Atividade: Discutindo o projeto do guia curricular de Computação Criativa
- Projetos do Estúdio Desenvolvendo Meu Projeto Final
    - https://scratch.mit.edu/studios/25033896/
    
### Reflexão
-  Qual feedback, se houver, você planeja incorporar em seu projeto?
- Qual projeto eu quero desenvolver?
-  Que passos eu darei para desenvolver meu projeto?
-
### Revisão
- Os alunos estão progredindo razoavelmente?
- Quais comentários ou sugestões você tem para os projetos?
- Cada aluno teve oportunidade de dar e receber feedback de várias fontes?
- Cada aluno registrou o roteiro de Feedback do Projeto no diário
- O projeto tem escopo adequado para a quantidade de tempo e recursos disponíveis para esse hackathon?
- Como você pode tornar os recursos acessíveis para os alunos que precisam deles?

### Dicas

- Todas as atividades de projeto são limitadas - pelo tempo, pelos recursos, por nossas próprias habilidades em um determinado momento - é necessário compromisso para realizá-las. Quais são os aspectos mais importantes? O que pode ser realizado no tempo restante?
- Os alunos podem ser extremamente valiosos para fornecer apoio e orientação uns aos outros durante todas as sessões do Scratch, e particularmente durante as sessões do hackaton. Incentivar os jovens a compartilhar seus conhecimentos e habilidades com os outros simplifica as coisas para o facilitador, mas também pode aprofundar significativamente o aprendizado e a compreensão dos criadores.
- Pessoas diferentes fornecerão perspectivas diferentes sobre o projeto em andamento. Crie oportunidades para os alunos receberem feedback de diversas fontes, incluindo eles mesmos!
- Embora o planejamento seja útil, não deve ser uma tarefa demorada nem a única maneira de fazer as coisas.
- Vários estilos de projeto e desenvolvimento devem ser incentivados e acomodados.

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
