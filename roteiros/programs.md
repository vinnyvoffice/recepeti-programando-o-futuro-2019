# Roteiro para Formação dos Alunos

## Informações
- 16 aulas
- 05 unidades
- 90 minutos por aula

# Escopo

> Com base em objetivos de aprendizagem do currículo CSTA revisão 2017 (https://www.csteachers.org/page/standards) 

- Programas que usam sequências e repetições simples para expressar ideias ou endereçar problemas
- Programas que incluem sequencias, eventos, repetições e condicionais
- Programas que usam variáveis e listas 
- Colaboração: comunidade, compartilhamento, reuso, modificação, incorporação de porções ou ideias
- Ética e Segurança Digital: atribuição de autoria; permissão de uso; licenças para imagem, som e conteúdo; comunicação digital; informações pessoais

## Aulas
- 01 Iniciando com o Scratch
- 02 Explorando o ambiente Scratch
- 03 Crie uma História
- 04 Jogo da Coleta
- 05 Jogo Pong 
- 06 Jogo de Pega-Pega: Félix e Herbert
- 07 Anime um Nome
- 08 Faça Música
- 09 Vamos Dançar
- 10 Jogo de Captura: Caça aos Dragões
- 11 Jogo Pega-Pega
- 12 Jogo de Pular
- 13 Jogo da Moda e Jogo da Corrida Olímpica
- 14 Jogo de Perguntas e Respostas: Turismo
- 15 Desenvolvendo Meu Projeto Final
- 16 Apresentando Meu Projeto Final

# Roteiro para Formação dos Educadores

## Informações
- 4 encontros
- 8 horas por encontro
- Objetivos de aprendizagem
- Conta de Professor Scratch
- Formulário de Feedback para as aulas com as cartilhas
- Avaliação de aprendizagem do aluno usando cartilhas e Kahoot!
- Recursos do guia curricular

## Encontros
- 01 Iniciando com o Scratch
- 01 Explorando o ambiente Scratch
- 01 Crie uma História
- 02 Jogo da Coleta
- 02 Jogo Pong 
- 02 Jogo de Pega-Pega: Félix e Herbert
- 03 Anime um Nome
- 03 Faça Música
- 03 Vamos Dançar
- 03 Jogo de Captura: Caça aos Dragões
- 03 Jogo Pega-Pega
- 03 Jogo de Pular
- 03 Jogo da Moda e Jogo da Corrida Olímpica
- 03 Jogo de Perguntas e Respostas: Turismo
- 04 Desenvolvendo Meu Projeto Final
- 04 Apresentando Meu Projeto Final

# Referências
- Novidades do Scratch 3: https://en.scratch-wiki.info/wiki/Scratch_3.0 
- Code.org e Computação no Ensino Fundamental: https://code.org/promote e https://studio.code.org/courses 
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Projetos Code Club: https://codeclubprojects.org 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
