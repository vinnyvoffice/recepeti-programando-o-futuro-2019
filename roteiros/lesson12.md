## Aula 12

### Unidade 
- 4

### Nome
- Jogo de Pular
- Animação Fatos Curiosos

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá criada um jogo em que o ator pula para desviar de obstáculos que se movem

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem seus jogos

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/jumping-cards.pdf
- Cartão da Animação Fatos Curiosos

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: cards-jump-card1 Pule
    - Cartão 2 resolvido: cards-jump-card2 Vá para o começo
    - Cartão 3 resolvido: cards-jump-card3 Crie obstáculos que se movem
    - Cartão 4 resolvido: cards-jump-card4 Adicione som
    - Cartão 5 resolvido: cards-jump-card5 Pare o jogo
    - Cartão 6 resolvido: cards-jump-card6 Adicione mais obstáculos
    - Cartão 7 resolvido: cards-jump-card7 Marque pontos
- Projetos do Estúdio Jogo de Pular
    - https://scratch.mit.edu/studios/25033860/

### Reflexão
- Seguir os cartões é suficiente para resolver as atividades?
- Quando o jogo termina?
- O que você achou mais difícil de fazer? 
- O que você achou foi mais fácil de fazer?
- O que você gostou no seu jogo? 
- O que você não gostou no seu jogo?
- O que você gostou em outros jogos que você viu no estúdio da aula?
- Você consegue fazer o ator pensar os fatos curiosos sem usar blocos personalizados? Se sim, qual é a melhor alternativa? Explique
- Você consegue fazer o ator pensar sem usar lista? Se sim, qual é a melhor alternativa? Explique

### Revisão
- Todos os participantes conseguiram criar seus jogos?
- Todos os participantes conseguiram criar a animação usando listas e blocos personalizados?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?

### Dicas
- Nesta atividade não existe guia do educador. A ideia é que nessa etapa do curso os participantes já consigam resolver a atividade apenas com o uso dos cartões.
- Faça a atividade orientada pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes
- Apesar de avançado o conceito de lista será utilizado na cartilha 3. Certifique-se de que os participantes conseguiram concluir a animação.
- O conceito de bloco personalizado é avançado mas não é obrigatório para nenhum atividade prevista.

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
