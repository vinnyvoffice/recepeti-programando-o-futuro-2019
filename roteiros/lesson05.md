## Aula 05

### Unidade 
- 2

### Nome
- Jogo Pong

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá desenvolvido um jogo no estilo pong no qual o jogador deve impedir que a bola atinja a borda usando uma raquete

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem seus jogos

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/pong
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/PongGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/pong-cards.pdf

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: pong-cards-card1 Crie movimento
    - Cartão 2 resolvido: pong-cards-card2 Mova a raquete
    - Cartão 3 resolvido: pong-cards-card3 Faça a bola quicar na raquete
    - Cartão 4 resolvido: pong-cards-card4 Faça o jogo acabar
    - Cartão 5 resolvido: pong-cards-card5 Marque pontos
    - Cartão 6 resolvido: pong-cards-card6 Exiba uma mensagem de vitória
- Projetos do Estúdio Jogo Pong
    - https://scratch.mit.edu/studios/25032653/
    - http://scratch.mit.edu/studios/644508/

### Reflexão
- O que precisa acontecer para que a bola seja rebatida pela raquete?
- O que precisa acontecer para que a bola seja identificada tocando a borda?
- O que precisa acontecer para que a mensagem de vitória seja exibida?

### Revisão
- Todos os participantes conseguiram criar seus jogos?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes


### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
