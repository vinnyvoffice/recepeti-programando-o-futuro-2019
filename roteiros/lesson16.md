## Aula 16

### Unidade 
- 5

### Nome
- Apresentando Meu Projeto Final 

### Tempo
- 90 a 180 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - irá trabalhar no esboço final do projeto e se preparar para a apresentação do projeto concluído
    -  compartilhará seus projetos finais com os colegas e refletirá sobre o processo de desenvolvimento e as experiências de criação computacional

### Etapas

- Lembre aos alunos de que eles compartilharão os projetos entre si (e possivelmente com os convidados) como forma de reconhecer o trabalho realizado e refletir sobre suas experiências. Explique que esta sessão é uma oportunidade para finalizar seus trabalhos e apresentar uma estratégia para compartilhar seus projetos com outras pessoas.
- Dê aos alunos tempo para trabalhar em seus projetos e se prepararem para a apresentação. Opcionalmente, colete trabalhos finais em andamento em um estúdio da classe para facilitar a apresentação. Opcionalmente, peça aos alunos que adicionem seus projetos ao Estúdio Hackathon.

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Leia os roteiros da aula
- Leia as questões da seção Revisão 
- Crie um clima comemorativo, convidando a comunidade, tocando música, enfeitando o espaço de apresentação e/ou oferecendo lanches.

#### Contextualizando (20 minutos)
- Estime os alunos lendo as questões das seção Reflexão
- Apresente o roteiro de Reflexões do Projeto para os participantes e oriente sobre o registro no diário individual

#### Praticando (20 minutos)
- Peça para que façam ajustes no projeto final antes da apresentação usando os feedbacks recebidos na aula anterior
- Peça para que registrem as respostas do roteiro no diário
- Peça para que compartilhem um resumo das respostas do roteiro com descrição do projeto
- Peça para que compartilhem o projeto final no estúdio da aula

#### Compartilhando (30 minutos)
- Oriente sobre a apresentação, indicando que cada um participante possui até 3 minutos incluindo leitura da descrição, demonstração do jogo e ideias para o futuro

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Roteiro: Reflexões do Projeto 

#### Complementares
- Atividade: Preparando a Apresentação do guia curricular de Computação Criativa
- Atividade: Apresentação do guia curricular de Computação Criativa
- Projetos do Estúdio Apresentando Meu Projeto Final

### Reflexão
-  Qual é o seu projeto?
-  Qual foi o seu processo para desenvolver o projeto? 
-  O que você quer criar em seguida?
- Revise seu caderno de projetos. Quais tipos de anotações você fez?
-  Quais notas foram mais úteis?
-  Qual foi o seu projeto de Scratch favorito para trabalhar
até agora? Por que é seu favorito?
-  O que você quer criar em seguida?

### Revisão
- Cada grupo ou aluno completou uma folha de Reflexões do Projeto?
- Os alunos tiveram oportunidade de compartilhar seu trabalho?

### Dicas
- Os alunos podem sentir-se ansiosos ou estressados ao concluir seus projetos. Esta é uma oportunidade para lembrá-los de que: (1) esta experiência é apenas uma etapa em seu aprendizado como programador, e (2) alguns tipos de estresse podem ser bons, nos ajudando a focar em nossos objetivos e fazer as coisas!
- Sugerimos até 3 minutos para apresentação para cada aluno incluindo demonstração, resposta aos comentários e ideias para o futuro

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards