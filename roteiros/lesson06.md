## Aula 06

### Unidade 
- 2

### Nome
- Jogo de Pega-Pega: Félix e Herbert

### Tempo
- 90 a 180 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - Terá criado o seu próprio jogo de pega-pega
    - Terá utilizado blocos de evento para associar comportamento aos atores
    - Terá utilizado blocos para movimentar os atores
    - Terá utilizado blocos  para alterar a aparência ou fazer um ator falar
    - Terá utilizado blocos para reproduzir um som de instrumento musical
    - Terá utilizado blocos condicionais e de repetição
    - Terá utilizado a transmissão e o recebimento de mensagem para que os atores de comuniquem
    - Terá utilizado uma variável para armazenar os pontos do jogador
    - Terá compartilhado seu jogo com a turma 
    - Terá contribuído com outros alunos da turma através de comunicação digital usando os comentários
    - Terá exercitado a ética digital ao seguir as diretrizes da comunidade Scratch

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Confirme se todos os participantes trouxeram a versão impressa da cartilha 1
- Leia as questões da seção Revisão 

#### Contextualizando (10 minutos)
- Estime os alunos lendo as questões das seção Reflexão
- Apresente o estúdio com projetos feitos pelos educadores
- Destaque a importância da atividade desta aula como fechamento da unidades 2

#### Praticando (50 minutos)
- Oriente os alunos a fazer o jogo guiados pela cartilha
- Oriente os alunos a criar um novo projeto caso tenham resolvido todos os itens e desejem promover modificações

#### Compartilhando (10 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem seus jogos

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Cartilha do Jogo Félix e Herbert
- Formulário de Feedback do Educador: https://docs.google.com/spreadsheets/d/1ejXuUSBOhjaH3taeYS36MGBr-KF-kbsds9pK0s_tfDU/edit?usp=sharing

#### Complementares
- Projeto da atividade Crie uma História
- Projeto da atividade Jogo da Coleta
- Projeto da atividade Jogo Pong
- Cartões e guias das atividades acima
- Projetos do Estúdio Jogo Herbert e Félix
    - https://scratch.mit.edu/studios/25032668/

### Reflexão
- Quando o jogo termina?
- O que você achou mais difícil de fazer? 
- O que você achou foi mais fácil de fazer?
- O que você gostou no seu jogo? 
- O que você não gostou no seu jogo?
- O que você gostou em outros jogos que você viu no estúdio da aula?
- Quais blocos você já usou nas aulas anteriores?
- Quais blocos você ainda não havia  usado nas aulas anteriores?

### Revisão
- Verifique se os itens da seção Teste Seu Jogo foram atendidos
- Verifique se os itens da seção Desafios forma resolvidos
- Verifique se os blocos da seção Todos Blocos Sugeridos foram marcados
- Registre na planilha de feedback, quantos participantes fizeram a atividade e quantos eram previstos
- Registre na planilha de feedback, por item da seção Teste Seu Jogo, quantos participantes NÃO o resolveram
- Registre na planilha de feedback, por desafio, quantos participantes NÃO o resolveram 

### Dicas
- Lembre-se de que as atividades dessa aula incentivam a autonomia do participante e a colaboração com os demais participantes
- Não resolva as dúvidas para o participante: incentive-os a procurar a resposta nos recursos já utilizados em outras aulas ou conversando com outros alunos
- Recolha as cartilhas para facilitar a identificação dos itens atendidos. Devolva as cartilhas quando os registros forem encerrados
- Revise os projetos das cartilhas com poucos itens marcados ou com todos os itens marcados 

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
