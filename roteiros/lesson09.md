## Aula 09

### Unidade 
- 3

### Nome
- Vamos Dançar

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá criado uma dança animada combinando música e passos de dança

### Etapas

##### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem suas animações

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/dance 
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/DanceGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/dance-cards.pdf

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: cards-dance-card01 Crie uma sequência de passos
    - Cartão 2 resolvido: cards-dance-card02 Repita uma série de passos
    - Cartão 3 resolvido: cards-dance-card03 Coloque música
    - Cartão 4 resolvido: cards-dance-card04 Faça cada ator dançar na sua vez
    - Cartão 5 resolvido: cards-dance-card05 Defina a posição inicial
    - Cartão 6 resolvido: cards-dance-card06 Crie um efeito de sombra
    - Cartão 7 resolvido: cards-dance-card07 Faça uma dança interativa
    - Cartão 8 resolvido: cards-dance-card08 Crie efeitos de cores
    - Cartão 9 resolvido: cards-dance-card09 Faça seu ator deixar um rastro
- Projetos do Estúdio Vamos Dançar
    - https://scratch.mit.edu/studios/25032660/

### Reflexão
- É possível dançar mudando as fantasias mas sem usar blocos de espera? Como pode ser feito?
- É possível obter o mesmo resultado do cartão Repita uma série de passos em usar o laço de repetição? Como pode ser feito?
- Qual a vantagem de tocar som em uma sequência separada daquela que define os movimentos?

### Revisão
- Todos os participantes conseguiram criar suas animações?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
