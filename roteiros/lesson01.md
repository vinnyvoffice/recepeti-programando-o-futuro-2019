## Aula 01

### Unidade 
- 1 

### Nome
- Iniciando com o Scratch

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - será introduzido à computação criativa com o ambiente Scratch através de um vídeo ou da
exploração de exemplos de projetos
    -  será preparado para imaginar possibilidades para seu próprio processo criativo baseado no Scratch
    - será capaz de seguir orientações da comunidade Scratch
    - terá iniciado um diário personalizado para registrar seus processos e reflexões
    -  terá experimentado o Scratch de forma prática

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm caderno se os diários forem físicos 
- Confirme se todos os participantes têm conta para blog se os diários forem digitais
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme se todos os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista aos vídeos 
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os alunos lendo as questões das seção Reflexão
- Apresente o vídeo da Quarta Revolução Industrial
- Peça a opinião dos alunos sobre o video
- Apresente o vídeo da página inicial do site oficial
- Peça a opinião dos alunos sobre o video
- Apresente exemplos de projetos interessantes e inspiradores 

#### Praticando (30 minutos)
- Realize a dinâmica de apresentação para que os participantes se conheçam
- Peça aos participantes que criem seu diário físico ou digital
- Informe a conta que cada um irá utilizar durante o curso
- Esclareça a conta é necessária para salvar projetos
- Esclareça que o e-mail da conta precisa ser confirmado para que seja possível compartilhar e comentar projetos
- Dê aos alunos 10 minutos para explorar a interface do Scratch de forma livre

#### Compartilhando (20 minutos)
- Peça para que compartilhem sua exploração no estúdio da aula
- Peça para que registrem no diário a apresentação e a primeira exploração da ferramenta
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem o resultado de suas explorações iniciais

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguindo as orientações da seção Revisão

### Recursos

#### Necessários
- Roteiro: Dinâmica de Apresentação Pensar, Conversar e Compartilhar
        - nome
        - como quer ser chamado
        - turma
        - bairro
        - cidade natal
        - programas que utiliza
        - jogos que jogam
        - sites que acessam
        - quadrinhos e livros que gostam de ler
        - programação
        - scratch
        - expectativas
- Vídeo: Quarta Revolução Industrial 
        - https://www.youtube.com/watch?v=jTLpqipsw0g  
- Vídeo: Visão Geral do Scratch 
        - https://scratch.mit.edu/about
        - https://scratch.mit.edu/studios/5826815/
- Página: Diretrizes da comunidade Scratch 
        - http://scratch.mit.edu/community_guidelines
- Roteiro: Brincando com o Scratch
- Estúdio: Projetos que interessam e inspiram o educador

#### Complementares
- Atividade: Introdução ao Scratch do guia curricular de Computação Criativa
- Atividade: Criando uma Conta do guia curricular de Computação Criativa
- Atividade: Criando seu Diário do guia curricular de Computação Criativa
- Atividade: Brincando com o Scratch do guia curricular de Computação Criativa
- Página: Sobre o Scratch
        -  https://scratch.mit.edu/about 
    
### Reflexão
- Quais são as diferentes formas de interagir com o computador?
- Quantas dessas formas envolvem um processo criativo?

### Revisão
- Os alunos discutiram uma gama diversificada de ideias de projetos? Se não, tente mostrar uma ampla variedade de projetos para dar aos alunos uma noção das possibilidades
- Os alunos gostaram da ideia de criar um diário pro curso?
- Os alunos sabem como iniciar um novo projeto?
- Os alunos entendem o mecanismo básico de agrupar os blocos Scratch?
- Os alunos adicionaram seus projetos ao estúdio da aula com sucesso?

### Dicas
- Sugerimos que as contas para cada aluno sejam criadas com a conta de professor. 
- Oriente a criação de contas pessoais caso os alunos tenham interesse de continuar a exploração em casa.
- Comente sobre a importância da ética digital e a existência da página de diretrizes da comunidade.

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards