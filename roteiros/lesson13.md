## Aula 13

### Unidade 
- 4

### Nome
- Jogo da Moda e Jogo da Corrida Olímpica

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá criado um jogo de moda no qual o jogador pode escolhar as roupas para vestir um personagem ou
    - terá criado uma corrida na qual o jogador compete contra o computador ou contra outro jogador

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte os estúdios da aula 
- Assista aos vídeos dos tutoriais
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente os vídeos dos tutoriais
- Peça a opinião dos participantes sobre os videos

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no respectivo estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem seus jogos

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/fashion
    - http://scratch.mit.edu/racegame
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/FashionGuide.pdf
    - https://resources.scratch.mit.edu/www/guides/pt-br/RaceGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards-2.0/pt-br/fashionCards.pdf
    - https://resources.scratch.mit.edu/www/cards-2.0/pt-br/raceCards.pdf

#### Complementares
- Cartões da Atividade: Jogo da Moda
    - Cartão 1 resolvido: cards-fashion-card1 Escolha um personagem
    - Cartão 2 resolvido: cards-fashion-card2 Brinque com as cores
    - Cartão 3 resolvido: cards-fashion-card3 Altere o estilo
    - Cartão 4 resolvido: cards-fashion-card4 Mude o pano de fundo
    - Cartão 5 resolvido: cards-fashion-card5 Exiba suas roupas
    - Cartão 6 resolvido: cards-fashion-card6 Deslize
    - Cartão 7 resolvido: cards-fashion-card7 Encaixe 
- Cartões da Atividade: Jogo Corrida Olímpica
    - Cartão 1 resolvido: cards-race-card1 Inicie a corrida
    - Cartão 2 resolvido: cards-race-card2 Em suas marcas
    - Cartão 3 resolvido: cards-race-card3 Linha de chegada
    - Cartão 4 resolvido: cards-race-card4 Escolha um competidor
    - Cartão 5 resolvido: cards-race-card5 Adicione um som
    - Cartão 6 resolvido: cards-race-card6 Anime a corrida
    - Cartão 7 resolvido: cards-race-card7 Competição
- Projetos dos Estúdios Jogo da Moda e Jogo Corrida Olímpica 
    - https://scratch.mit.edu/studios/25032664/
    - https://scratch.mit.edu/studios/25032666/

### Reflexão
- Você notou que os cartões dessa atividade mostram blocos diferentes dos que estão no estúdio? Esses blocos da versão 2 do Scratch 
- Quais blocos mudaram de cor?
- Quais blocos mudaram de nome?
- Quando o jogo termina?
- O que você achou mais difícil de fazer? 
- O que você achou foi mais fácil de fazer?
- O que você gostou no seu jogo? 
- O que você não gostou no seu jogo?
- O que você gostou em outros jogos que você viu no estúdio da aula?

### Revisão
- Os participantes conseguiram concluir as atividades mesmo os guias sendo a versão 2 do Scratch? 
- Todos os participantes conseguiram criar seus jogos?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Incentive os participantes a registrar no diário as diferenças entre os editores das versões 2 e 3 do Scratch
- Incentive os participantes a registrar no diário as diferenças nos blocos apresentados nos cartões e aqueles encontrados no editor
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes


### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
