## Aula 07

### Unidade 
- 3

### Nome
- Anime um Nome

### Tempo
- 90 minutos

### Objetivos
- Ao completar esta aula, o participante: 
    - terá aprendido a realizar animações nas letras do seu nome ou de uma palavra de sua preferência

### Etapas

#### Preparando (10 minutos)
- Confirme se o computador do professor está acessando o site do Scratch e os recursos online
- Confirme se o projetor está disponível e funcionando
- Confirme se todos os participantes têm contas Scratch
- Confirme se os computadores estão acessando o site do Scratch 
- Confirme que os recursos estão disponíveis para uso 
- Monte o estúdio da aula 
- Assista ao vídeo do tutorial
- Leia as questões da seção Revisão 

#### Contextualizando (20 minutos)
- Estime os participantes lendo as questões das seção Reflexão
- Apresente o tema e troque ideias para o projeto seguindo as orientações da seção Imagine no guia do educador
- Apresente o vídeo do tutorial
- Peça a opinião dos participantes sobre o video

#### Praticando (30 minutos)
- Auxilie os participantes durante a atividade seguindo as orientações da seção Crie no guia do educador
- Comente a existência de variações na subseção E Agora? no guia do educador

#### Compartilhando (20 minutos)
- Peça para que compartilhem o projeto no estúdio da aula
- Peça para que registrem as atividades da aula no diário 
- Peça que 3 voluntários revelem o que escreveram em seus diários
- Peça que 3 voluntários apresentem suas animações

#### Recapitulando (10 minutos)
- Revise o trabalho dos alunos seguinte as orientações da seção Revisão

### Recursos

#### Necessários
- Vídeo do Tutorial 
    - http://scratch.mit.edu/name
- Guia da Atividade
    - https://resources.scratch.mit.edu/www/guides/pt-br/NameGuide.pdf
- Cartões da Atividade
    - https://resources.scratch.mit.edu/www/cards/pt-br/name-cards.pdf

#### Complementares
- Cartões da Atividade
    - Cartão 1 resolvido: cards-name-card1 Mude a cor ao clicar
    - Cartão 2 resolvido: cards-name-card2 Gire
    - Cartão 3 resolvido: cards-name-card3 Reproduza um som
    - Cartão 4 resolvido: cards-name-card4 Faça uma letra dançar
    - Cartão 5 resolvido: cards-name-card5 Altere o tamanho
    - Cartão 6 resolvido: cards-name-card6 Pressione uma tecla
    - Cartão 7 resolvido: cards-name-card7 Deslize
- Projetos do Estúdio Anime um Nome
    - https://scratch.mit.edu/studios/25032654/

### Reflexão
- Qual bloco precisa ser utilizado para que a cor da letra mude?
- Qual bloco precisa ser utilizado para que o tamanho da letra mude?
- O que precisou ser feito para a letra dançar?

### Revisão
- Todos os participantes conseguiram criar suas histórias?
- Houve dificuldade na utilização de algum bloco?
- Houve dificuldade na utilização da algum cartão?
- Algum participante optou pelas variações da subseção E agora? 

### Dicas
- Faça a atividade orientada pelo guia e pelos cartões pelo menos uma vez antes da aula permitindo relembrar detalhes do ambiente ou do material que ajudem na facilitação
- Apresente a atividade resolvida disponível na seção de Recursos Complementares ou a sua própria solução caso os participantes peçam ou você veja oportunidade de mostrar alternativas diferentes

### Referências
- Scratch para Educadores: https://scratch.mit.edu/educators/ 
- Lições Code Club: https://codeclubprojects.org/en-GB/scratch/
- Guia Curricular de Computação Criativa: http://creativecomputing.gse.harvard.edu/guide/
- Currículo de Ciências da Computação proposto pela CSTA revisão 2017: https://www.csteachers.org/page/standards
