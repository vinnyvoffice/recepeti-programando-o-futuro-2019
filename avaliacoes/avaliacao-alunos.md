# Avaliação de Aprendizagem dos Alunos

## Unidade 1
- 01 Iniciando com o Scratch
- 02 Explorando o ambiente Scratch

## Unidade 2
- 03 Crie uma História
- 04 Jogo da Coleta
- 05 Jogo Pong 
- 06 Jogo de Pega-Pega: Félix e Herbert

## Unidade 3
- 07 Anime um Nome
- 08 Faça Música
- 09 Vamos Dançar
- 10 Jogo de Captura: Caça aos Dragões

## Unidade 4
- 11 Jogo Pega-Pega
- 12 Jogo de Pular
- 13 Jogo da Moda e Jogo da Corrida Olímpica
- 14 Jogo de Perguntas e Respostas: Turismo

## Unidade 5
- 15 Desenvolvendo Meu Projeto Final
- 16 Apresentando Meu Projeto Final
