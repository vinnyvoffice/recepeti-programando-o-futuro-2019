# Criando o jogo: Turismo na Escola com o Scratch 

## Começando

Chegou a hora de criarmos um jogo de perguntas e respostas. O jogador terá 3 minutos para responder o maior número de questões sobre turismo e sobre os atrativos da Costa Verde Mar. Ao acertar a resposta o jogador ganha 1 ponto, porém, ao errar a resposta o jogador tem 1 ponto subtraído.

`imagem do jogo finalizado`

* [ ] 1. Antes de começar certifique-se que o Scratch esteja em `português`. Se não for o caso clique no globo, `ícone correspondente` em cima, no canto esquerdo da tela e escolha `Português (Brasil)``.

## Passo 1: Criar uma lista de perguntas e respostas

* [ ] 1. Crie um novo projeto Scratch.
* [ ] 2. Escolha um personagem e um plano de fundo para o seu jogo.
* [ ] 3. Crie uma lista chamada `questões`, com todos as perguntas criadas.
* [ ] 4. Crie uma nova lista chamada `respostas`, com as respostas correspondentes às perguntas da lista acima.
* [ ] 5. Crie uma variável chamada `pergunta``, para definir qual a pergunta apresentada ao jogador. 
* [ ] 6. Arraste e encaixa estes blocos para a área de roteiros:

```
 quando clicar na Bandeira
    sempre
        escolha <item> para escolher randomicamente entre uma das <questões>
        pergunte o <item> de <questões> e espere
        se resposta = <item> de <respostas> então
            diga Isso ai. Parabéns!
        senão
            diga Humm. Resposta errada.

```

Teste o projeto
Clique na bandeira verde.

* [ ] Lembre-se de responder uma pergunta corretamente e de errar outra.
* [ ] Desafio: Você consegue criar um contador de tempo para o palco? Lembre-se de converter os minutos em segundos.
* Salve o projeto

## Passo 2: Conte os pontos

Vamos criar um placar, para sabermos se estamos indo bem e podermos comprar os resultado. O placar começa do zero e aumenta um ponto a cada resposta certa. Lembre-se que o placar também será diminuído a cada resposta errada. 

* [ ] 1. Clique em variáveis e crie uma variável chamada `placar`. Mantenha selecionada a opção `Para todos os atores`
* [ ] 2. Clique no placo e crie este conjunto de blocos. Atenção: Certifique-se de que os blocos sejam criados para o placo, clicando sobre ele no canto esquerdo da tela, antes de começa a encaixar:
```
quando clicar na Bandeira
    mude <placar> para (0)
    se resposta = <item> de <respostas> então
        adicione a <placar> 1
    senão
        diminua a <placar> 1     
    sempre    
```

Teste o projeto
Clique na bandeira verde.

* 1. Os pontos aumentam com os acertos?
* 2. Os pontos diminuem com os erros?
* 3. O placar é zerado, toda vez que um novo jogo começa? 
* Salve o projeto

## Passo 3: Várias partidas

* 1. Crie um ator para o botão de jogar, no qual seu jogador irá clicar para começar uma partida. Você pode desenhar seu próprio botão ou editar algum que já exista na biblioteca. Adicione este código ao botão:

```
    quando clicar na Bandeira
        mostre
        quando este ator for clicado
        esconda
        envio INICIO a todos
```
* 2. Edite o código do seu personagem, para que o jogo comece quando receber a mensagem início e não quando a bandeira for clicada.

```
quando receber INICIO
    mude <placar> para 0
```

Teste o projeto
Clique na bandeira verde.

* [ ] 1. Clique na bandeira verde e então no seu novo botão para testá-lo. O jogo não deve começar até que você clique no botão.
* [ ] 2. Vocês percebeu que o contador de tempo é atividade quando a bandeira verde é clicado e não quando o jogo começa? Como corrigir? 
* Salve o projeto

## Passo 4: Adicionando gráficos

Invés do seu personagem apenas dizer se a resposta está correta ou errada, vamos adicionar alguns efeitos gráficos que farão o jogador saber como está indo.

* [ ] 1. Crie um novo ator e chame-o de `resultado`. Vocês pode escolher as imagens da biblioteca ou criar as suas imagens que correspondam aos acertos e aos erros, dando nomes para as imagens. `imagens de acerto e erro`
* [ ] 2. Mude o código do seu personagem, então, ao invés de dizer ao jogador como ele está indo, irá enviar a mensagem correto e errado.

```
    quando clicar na Bandeira
        sempre
            escolha <item> para escolher randomicamente entre uma das <questões>
            pergunte o <item> de <questões> e espere
            se respota = <item> de <respostas> então
                mude para a fantasia FELIZ
                diga *Isso ai. Parabéns!* 
                adicione a <placar> 1
                envie CORRETO a todos
            senão
                mude para fantasia FELIZ
                diga *Humm. Resposta errada.
                envie ERRADO a todos
```

Teste o projeto
Clique na bandeira verde. 

* [ ] 1. Foram exibidas as imagens de acertos e erros?
* Desafio: você conseguiria melhorar a animação dos gráficos?
* Salve o projeto 

## Passo 4: Lista de desafios

Com a ajuda do seu professor, vamos aumentar a jogabilidade e incluir alguns diferenciais no seu jogo?

* [ ] Desafio 1. Será que conseguiríamos alterar as fantasias do personagem, de acordo com as respostas?
* [ ] Desafio 2. Conforme o tempo vai passando, conseguiríamos alterar os fundos do nosso jogo?
* [ ] Desafio 3. Vamos adicionar sons ao jogo? Por exemplo, tocar um determinado som quando o jogador acerta ou erra uma questão
* [ ] Desafio 4. Vamos adicionar sons ao jogo? Por exemplo, tocar um determinado som, conforme o tempo vai se esgotando?
* [ ] Desafio 5. Invés de responde o máximo de questões possíveis nos 3 minutos, será que conseguiríamos alterar a regra do jogo, para ver qual jogador leva menos tempo para responder corretamente 10 perguntas? 

Teste o projeto
Clique na bandeira verde.

Ficará mais fácil se você implementar um desafio por vez. Após cada implementação, execute os testes e veja se o resultado está conforme o desejado.
Converse com o seu professor e proponha outros desafios. O seu jogo pode ficar ainda mais 

## Parabéns

Parabéns. Você criou um jogo sobre o turismo e ajudou a apresentar as belezas e curiosidades de nossa região aos jogadores. Lembre-se que você pode compartilha este jogo com amigos e familiares.

# Recursos utilizados

| Nome           | Tipo?        | Função  |
| :------------- |:-------------| :-----  |
| adição de 2 operandos | Operadores | . | 
| diga \[*texto\*] | Aparência | . | 
| envie <*mensagem*> a todos | Aparência | . | 
| esconda | Aparência | . | 
| item (*número*) de \[*lista*\] | Variáveis | . | 
| mostre | Aparência | . | 
| mude \[*nome*\] para (*número*) | Variáveis | . | 
| mude para fantasia \[*nome*\] | Aparência | . | 
| número aleatório entre (*número*) e (*número*) | Operadores | . | 
| pergunte *texto* e espere | Sensores | . | 
| quando clicar na bandeira | Eventos | definir que blocos devem ser executados quando o jogo começa | 
| quando receber <*mensagem*> | ? | . | 
| se <*condição*> então senão | Controle | . | 
| sorteie número entre (*número*) e (*número*) | Operadores | . | 
| subtração de 2 operandos | Operadores | . | 
