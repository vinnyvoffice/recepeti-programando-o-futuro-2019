# Cartilha 3

Criando o jogo Turismo na Escola com o Scratch.

## Apresentando 

Vamos criar um jogo de perguntas e respostas. O jogador terá 3 minutos para responder o maior número de questões sobre turismo e sobre os atrativos da Costa Verde Mar. Ao acertar a resposta o jogador ganha 1 ponto. Porém ao errar a resposta o jogador perde 1 ponto.

`#paracegover imagem do jogo Turismo na Escola pronto`

## Preparando 

* [ ] Você já deve ter feito o Jogo de Pega Pega. Ele utiliza espera com condição, troca de cenário e variáveis.
* [ ] Você já deve ter feito o Jogo de Pular. Ele utiliza repetição, parada de roteiros e espera por tempo.
* [ ] Você já deve ter feito o Jogo da Moda. Ele utiliza efeito de cor, troca de fantasia, se então, e evento de clique no ator.
* [ ] Você já deve ter feito a animação Fatos Curiosos. Ela utiliza listas e variáveis.
* [ ] Você deve definir 10 perguntas e respostas sobre turismo. 
* [ ] Confirme que o idioma do Scratch é Português Brasileiro 

``` #paracegover imagem do idioma em Português Brasileiro```

Dica: Você pode registrar o seu progresso marcando o quadro ao lado de cada item.

## Passo 1: Jogador responde perguntas

Vamos definir as perguntas e as respostas sobre turismo. E fazer com que o ator faça as perguntas e diga se a resposta está ou não certa.

### Orientações

* [ ] 1. Crie um novo projeto. 
* [ ] 2. Escolha um cenário para o jogo. Nossa sugestão é o `Interior > Room 1`.
* [ ] 3. Escolha um novo ator. Nossa sugestão é o `Imaginários > Giga`. 
* [ ] 4. Crie uma lista chamada **perguntas** adicionando cada uma das perguntas definidas. 
* [ ] 5. Crie outra lista chamada **respostas** adicionando cada uma das respostas na mesma sequencia usada para as perguntas.
* [ ] 6. Crie uma variável **posicao** para definir de qual posicao da lista a pergunta será escolhida.
* [ ] 7. Crie uma variável **pergunta** para definir qual a pergunta apresentada ao jogador.
* [ ] 8. Crie uma variável **resposta** para definir qual a resposta para a pergunta apresentada ao jogador.
* [ ] 9. Crie uma variável **cronometro** para definir o tempo do jogo.
* [ ] 10. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o cenário

```
quando o jogo iniciar
    O cenário deve guardar o valor 180 para cronometro
    as ações abaixo sempre devem ser repetidas
        O cenário deve diminuir o valor 1 do cronometro
        O cenário deve esperar um segundo


quando o jogo iniciar
    O cenário deve esperar até que o cronometro seja menor ou igual a zero
    O cenário deve parar todos os roteiros
```

#### Para o Giga

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Giga deve escolher um valor aleatório para guardar em posição
        Giga deve guardar a pergunta da posição escolhida
        Giga deve guardar a resposta da posição escolhida
        Giga deve perguntar ao jogador
        se a resposta do jogador = resposta da posição escolhida então
            Giga deve dizer que acertou
        senão 
            Giga deve dizer que errou
        Giga deve esperar metade de um segundo
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

    * [ ] A mensagem correta deve ser apresentada quando a resposta está certa.
    * [ ] A mensagem correta deve ser apresentada quando a resposta está errada.
    * [ ] O cronometro deve iniciar em 180 e diminuir a cada segundo.
    * [ ] O jogo deve parar quando o cronometro zerar.

``` #paracegover imagem do passo 1 do jogo pronto```

## Passo 2: Jogador ganha pontos quando acerta e perde pontos quando erra 

Vamos adicionar um ponto a cada resposta certa. E retirar um ponto a cada resposta errada. 

### Orientações

* [ ] 1. Crie uma variável chamada **pontos** para todos os atores.
* [ ] 2. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o Giga

```
quando o jogo iniciar
    Giga deve definir o valor 0 para pontos
    as ações abaixo sempre devem ser repetidas
        Giga deve escolher um valor aleatório para guardar em posição
        Giga deve guardar a pergunta da posição escolhida
        Giga deve guardar a resposta da posição escolhida
        Giga deve perguntar ao jogador
        se a resposta do jogador = resposta da posição escolhida então
            Giga deve dizer que acertou
            Giga deve adicionar o valor 1 a pontos
        senão 
            Giga deve dizer que errou
            Giga deve retirar o valor 1 de pontos
        Giga deve esperar metade de um segundo
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] O jogador deve ganhar 1 ponto a cada acerto.
* [ ] O jogador deve perder 1 ponto a cada erro.
* [ ] Os pontos devem ser zerados a cada nova partida.

``` #paracegover imagem do passo 2 do jogo pronto```

## Passo 3: Jogo inicia somente quando botão Jogar é clicado

O Giga só começa fazer perguntas quando o botão Jogar for cliado. 

### Orientações

* [ ] 1. Adicione um botão desenhado ou escolha um pronto da biblioteca de atores. O botão deve ter o texto "Jogar".
* [ ] 2. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o botão Jogar

```
quando o jogo iniciar
    O botão deve se mostrar

quando este ator for clicado
    O botão deve enviar mensagem jogar
    O botão deve se esconder
```

#### Para o Giga

```
quando o jogo iniciar
    Giga deve se esconder

quando mensagem jogar for recebida
    executar as ações que eram feitas no passo 2 ao iniciar o jogo
```

#### Para o cenário

```
quando a mensagem jogar for recebida
    executar as ações que eram feitas no passo 1 ao iniciar o jogo

quando o jogo iniciar
    O cenário deve guardar o valor zero em cronometro
    O cenário deve guardar o valor zero em pontos
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] Botão Jogar deve aparecer quando o jogo inicia
* [ ] Giga deve desaparecer quando o jogo inicia
* [ ] Giga só deve começar a perguntar quando o botão Jogar é clicado
* [ ] O cronometro só deve começar quando o botão Jogar é clicado
* [ ] O cronometro deve zerar quando o jogo inicia
* [ ] Os pontos devem zerar quando o jogo inicia

``` #paracegover imagem do passo 3 do jogo pronto```

## Passo 4: Jogo mostra animação quando jogador acerta ou erra

Vamos mostrar mensagens animadas quando o jogador acertar a resposta e quando o jogador errar a resposta.

### Orientações

* [ ] 1. Adicione um novo ator chamado **acerto**. Nossa sugestão é o `Tudo > Button4`. Você também pode desenhar seu próprio botão de acerto.
* [ ] 2. Adicione um outro ator chamado **erro**. Nossa sugestão é o `Tudo > Button5`. Você também pode desenhar seu próprio botão de erro.
* [ ] 3. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o Giga

```
quando a mensagem jogar for recebida
   Giga deve se mostrar
   Giga deve definir o valor 0 para pontos
    as ações abaixo sempre devem ser repetidas
        Giga deve escolher um valor aleatório para guardar em posição
        Giga deve guardar a pergunta da posição escolhida
        Giga deve guardar a resposta da posição escolhida
        Giga deve perguntar ao jogador
        se a resposta do jogador = resposta da posição escolhida então
            Giga deve transmitir a mensagem "acertou"
            Giga deve dizer que acertou
            Giga deve adicionar o valor 1 a pontos
        senão 
            Giga deve transmitir a mensagem "errou"
            Giga deve dizer que errou
            Giga deve retirar o valor 1 de pontos
        Giga deve esperar metade de um segundo

```

#### Para o botão acerto
```
quando a mensagem acertou for recebida
    repita 10 vezes os blocos abaixo
        O botão deve aparecer
        O botão deve esperar 0.1 segundos
        O botão deve desaparecer
        O botão deve esperar 0.1 segundos
```

#### Para o botão erro
```
quando a mensagem errou for recebida
    repita 10 vezes os blocos abaixo
        O botão deve aparecer
        O botão deve esperar 0.1 segundos
        O botão deve desaparecer
        O botão deve esperar 0.1 segundos
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] Os botões acerto e erro não devem ser apresentados no início do jogo.
* [ ] Os botões acerto e erro não devem ser apresentados quando o botão Jogar for clicado.
* [ ] A animação de resposta certa deve ser apresentada no momento correto.
* [ ] A animação de resposta errada deve ser apresentada no momento correto.

``` #paracegover imagem do passo 4 do jogo pronto```

## Passo 5: Jogador vence se marcar mais de 5 pontos

Vamos considerar vitória se o jogador marcar mais de 5 pontos durante jogo.

### Orientações

* [ ] 1. Um som para vitória deve ser tocado se o jogado vencer. Nossa sugestão é o `Win`.
* [ ] 2. Um som para derrota deve ser tocado se o jogador perder. Nossa sugestão é o `Lose`.
* [ ] 3. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o cenário

```
quando o jogo iniciar
    O cenário deve guardar o valor zero em cronometro
    O cenário deve guardar o valor zero em pontos

quando a mensagem jogar for recebida
        O cenário deve guardar o valor 180 para cronometro
        as ações abaixo sempre devem ser repetidas
            O cenário deve diminuir o valor 1 do cronometro
            O cenário deve esperar um segundo

quando a mensagem jogar for recebida
    O cenário deve esperar até que o cronometro seja menor ou igual a zero
    se pontos maior que 5 então
        O cenário deve tocar o som para vitória
    senão 
        O cenário deve tocar o som para derrota
    O cenário deve parar todos os roteiros

```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar


## Desafios 

### Orientações

Resolva um desafio e clique no ícone `bandeira verde` para iniciar o jogo e testar. 

* [ ] 1. Tente alterar as fantasias do personagem de acordo com a resposta.
* [ ] 2. Tente alterar o cenário quando o jogo estiver acabando.
* [ ] 3. Tente tocar um som quando o jogo estiver acabando. Nossa sugestão é o `Clock Ticking`.
* [ ] 4. Tente mostrar o cronometro em minutos e segundos.
* [ ] 5. Tente alterar o jogo para guardar a maior pontuação em uma partida.

### Todos os blocos sugeridos

Abaixo marque os blocos utilizados em cada etapa.
...

## Parabéns

Você criou um jogo sobre o turismo e ajudou a apresentar as belezas e as curiosidades de nossa região aos jogadores.
Não esqueça que você pode compartilhar seu jogo com todos os seus amigos e família clicando em `Compartilhar`.

## Notas do aluno

# Resumo

## Etapas

* Apresentando
* Preparando 
* Passo 1: Jogador responde perguntas
* Passo 2: Jogador ganha pontos quando acerta e perde pontos quando erra 
* Passo 3: Jogo inicia somente quando botão Jogar é clicado
* Passo 4: Jogo mostra animação quando jogador acerta ou erra
* Passo 5: Jogador vence se marcar mais de 5 pontos
* Desafios

## Elementos

### Palco

* 2 cenários: sala, recorde

    quando ícone verde for clicado 
        mude para o cenário (sala)
        esconda a variável (minutos)
        esconda a variável (segundos)
        mude (cronometro) para (999)
        espere até que ((cronometro < 0) ou (cronometro = 0))
        trasmita (parar)
        se (pontos > 5) então
            toque o som (Win)
        senão
            toque o som (Lose)
        mude (pontos-ultima-partida) para (pontos)
        se (pontos > pontos-recorde) então
            mude (pontos-recorde) para (pontos)
            mude (usuario-recorde) para (nome de usuário)
            mude para o cenário (recorde)
            toque o som (Triumph) até o fim
        pare (todos)

    quando eu receber (iniciar) 
        mude (cronometro) para (180)
        mude (minutos) para (0)
        mude (segundos) para (0)
        mostre a variável (minutos)
        mostre a variável (segundos)
        sempre
            adicione (-1) a (cronometro)
            mude (minutos) para ((arredondamento para baixo de) de (cronometro) / 60)
            mude (segundos) para (resto de (cronometro) por 60)
            espere (1) seg

### Atores

#### Ator 1: inicio

    quando ícone verde for clicado
        mostre

    quando este ator for clicado
        transmita (iniciar)
        esconda
        
#### Ator 2: Giga

    quando eu receber (iniciar)
        mostre
        sempre
            mude (posicao) para (numero aleatório entre (1) e (tamanho de (resposta)))
            mude (pergunta) para (item (posicao) de (questoes))
            mude (resposta) para (item (posicao) de (respostas))
            pergunte (pergunta) e espere
            se (resposta do jogador = resposta) então
                transmita (acertou)
                diga (Você acertou :) )
                adicione (1) a (pontos)
            senão
                transmita (errou)
                diga (Você errou :( )
                adicione (-1) a (pontos)
            espere (0.5) seg

    quando eu receber (parar)
        pare (outros scripts no ator)


#### Ator 3: acerto

    quando ícone verde for clicado
        esconda

    quando eu receber (acertou)
        mostre
        repita (10) vezes
            próxima fantasia
        esconda

#### Ator 4: erro

    quando ícone verde for clicado
        esconda

    quando eu receber (errou)
        mostre
        repita (10) vezes
            próxima fantasia
        esconda

### Variáveis 

* Variável 1: pontos-recorde
* Variável 2: pontos-ultima-partida
* Variável 3: usuario-recorde
* Variável 4: cronometro
* Variável 5: minutos
* Variável 6: pergunta
* Variável 7: pontos
* Variável 8: posicao
* Variável 9: resposta
* Variável 10: resposta
* Variável 11: segundos

## Blocos Utilizados

* Variáveis > adicione **número** a variável
* Movimento > aponte para **ator**
* Movimento > aponte para **ponteiro do mouse**
* Controle > espere **número** segundos
* Movimento > mova **número** passos 
* Variáveis > mude **variável** para **número**
* Aparência > próxima fantasia
* Aparência > esconda
* Aparência > mostre
* Operadores > número aleatório entre **número1** e **número2**
* Controle > pare todos
* Eventos > quando **bandeira verde** for clicado
* Eventos > quando eu receber **mensagem**
* Eventos > quando este ator for clicado
* Música > toque instrumento **nome** por **número** segundos
* Som > toque o som **nome**
* Eventos > transmita **mensagem**
* Controle > repita até **condição**  
* Controle > se **condição** então 
* Movimento > se tocar na borda, volte
* Controle > sempre
* Movimento > vá para **ator**
* Movimento > vá para **ponteiro do mouse**
