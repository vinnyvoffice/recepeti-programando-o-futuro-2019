# Cartilha 1

Criando o Jogo Félix e Herbert com o Scratch 

## Começando

Nós vamos cria um jogo de pega-pega com o gato Félix e o rato Herbert. Você controla o Herbert com o mouse e tenta fugir do Félix. Quanto mais tempo você ficar sem ser pego, mais pontos você ganhará. Cuidado, se for pego, perderá pontos!

`Imagem de um exemplo de solução para o jogo`

* [ ] 1. Antes de começar certifique-se que o stratch esteja em `português`. Se não for o caso clique no globo, ícone em cima, no canto esquerdo da tela e escolha `português (brasil)` 

## Passo 1: Félix persegue o ponteiro do mouse

Acompanhe o seu progresso marcando os quadros abaixo.

* [ ] 1. Crie um novo projeto. `Arquivo - Novo`
* [ ] 2. Clique em Palco, na área em baixo à esquerda da tela. Em seguida, vá até a aba `Panos de Fundo`, importar fundo da biblioteca `ícone correspondente` e escolha o fundo interior/Hall.
* [ ] 3. Clique no Félix (no canto esquero, em baixo da tela) e depois clique no ícone azul `ícone correspondente`
* [ ] 4. Mude o nome do ator para Félix.
* [ ] 5. Certique-se que o `Estilo de rotação` do Félix é Esquerda-Direita 

`Imagem da seção de edição de atores`

* [ ] 6. Arraste e encaixe estes blocos para a área de roteiros na direita da tela:

```
quando clicar na bandeira
    sempre
        aponte para [ponteiro do mouse v]
        mova (10) passos
        próxima fantasia
        toque o tambor (62) por (0.3) batidas    
    fim         
```
* [ ] Teste o projeto. Clique na bandeira verde. 
   * [ ] O Félix está seguindo o ponteiro do mouse? 
   * [ ] Ele mexe as pernas enquanto se movimenta?
   * [ ] A velocidade que ele se move é correta?
   * Salve o projeto   

## Passo 2: Félix persegue o Herbert

Agora vamos fazer com que o Félix persiga o Herbert ao invés de perseguir o mouse.

* [ ] 1. Crie um `Novo Ator`, clicando em importar ator da biblioteca `ícone correspondente` (na área em baixo à esquerda) e esoclha animais/mouse1
* [ ] 2. Mude o nome do ator para Herbert 

`Imagem da seção de edição de atores`

* [ ] 3. Certifique-se de que o estilo de rotação do Herbert é Esquerda-Direita
* [ ] 4. Clique no botão Reduzir `ícone correspondente` que fica junto com as ferramentas na parte de cima da tela `barra de ícones`
* [ ] 5.  Arraste e encaixe os blocos seguintes para o Herbert `(Atenção: Certifique-se de que os blocos são criados para o Herbert, clicando 2x sobre ele antes e começar a encaixar)`: 
```
quando clicar na Bandeira
    sempre
        vá para [ponteiro do mouse v]
        aponte para [Felix v]
    fim    
```
* [ ] Teste o projeto. Clique na bandeira verde. 
   * [ ] O Herbert se move com o ponteiro do mouse?
   * [ ] O Félix está perseguindo o Herbert?
   * Salve o projeto   

## Passo 3: Diga "Te Peguei!"

Vamos fazer com que o Félix diga quando ele pegou o Herbert 
* [ ] 1. Modifique o roteiro do Félix pra que ele fiquem assim (Não apague os blocos existentes, apenas encaixa os blocos que estão faltando): 

`Atenção: Certifique-se de que os blocos são criados para o Félix, clicando 2x sobre ele antes de começar a encaixar`

```
quando clicar na Bandeira
    sempre
        aponte para [ponteiro do mouse v]
        mova (10) passos
        próximo fantasia
        toque o tambor (62) por (0.3) batidas
        se <tocando em [Herbert v]>
            diga [Te peguei!] por (1) segundos
        fim
    fim    
```
* [ ] Teste o projeto. Clique na bandeira verde. 
   * [ ] O Félix diz algo quando pega o Herbert?
   * Salve o projeto   

## Passo 4: Virando fantasma

Ao invés de o Féliz dizer algo, nós vamos fazer com que o Herbert vire um fantasma quando for pego. 

* [ ] 1. Modifique os blocos do Félix para enviar uma mensagem quando pegar o Herbert.
```
quando clicar na Bandeira
    sempre
        aponte para [ponteiro do mouse v]
        mova (10) passos
        próximo fantasia
        toque o tambor (62) por (0.3) batidas
        se <tocando em [Herbert v]>
            envie [pego v] para todos
            toque o tambor (58) por (0.2) batidas
            espere (1) segundos
        fim
    fim
```
* [ ] 2. Clique sobre o Herbert, vá até a aba fantasias, clique em importar fantasia da biblioteca `ícone correspondente` e escolha a fantasia imaginários/ghost2-b.
* [ ] 3. Clique no botão reduzir `ícone correspondente` para diminuir o tamanho da fantasia.
* [ ] 4. Mude o nome das fantasias do Herbert, de maneira que o rato (mouse1) se chame `vivo` e o fantasma (ghost2-b) se chame `morto`.
* [ ] 5. Crie um novo conjunto de blocos para o Herbert, que irá fazer ele virar fantasma. Estes blocos ficarão ao lado dos blocos já existentes para o Herbert (Não apague o que já existe!).

`Atenção: Certifique-se de que os blocos são criados para o Herbert, clicando 2x sobre ele antes de começar a encaixar`
```
quando receber [pego v]
    mude para a fantasia [morto v]
    espere (0.5) segundos
    mude para a fantasia [vivo v]
```
* [ ] Teste o projeto. Clique na bandeira verde. 
    * [ ] O Herbert vira fantasma quando pego?
    * [ ] O Félix toca o som carto no momento certo?
    * [ ] O Félix ca parado tempo su ciente pra o Herbert fugir?
    * Salve o projeto   
## Passo 5: Conte os pontos

Vamos criar um placar para saber se estamos indo bem. O placar começa do zero e aumenta um ponto a cada segund. Quando o Félix pegar o Herbert, o jogador perderá cem pontos.

* [ ] 1. Clique em `variáveis` e crie uma variável chamada placar. Mantenha selecionada a opção `"para todos os atores"`. 
* [ ] 2. Clique no palco e crie testes dois conjuntos de blocos. `Atenção: Certifique-se de que os blocos são criados para o palco, clicando sobre ele no canto esquerdo da tela antes de começar a encaixar`

```
    quando clicar na bandeira
        mude [placar v] para (0)
        sempre
            mude [placar v] para (1)
            espere (1) segundos
        fim


    quando receber [pego v]
        mude [placar v] por (-100)    
```
* [ ] Teste o projeto. Clique na bandeira verde. 
    * [ ] Os pontos aumentam um a um a cada segundo?
    * [ ] Os pontos diminuem de 100 quando o Herbert é pego?
    * [ ] O que acontece quando o Herbert é pego antes que o placar chegue a 100?
    * [ ] O placar volta a zero quando você começa um novo jogo?
    * Salve o projeto   

## Parabéns

Você acaba de criar o seu primeiro jogo!. Não esqueça que você pode compartilhar seu jogo com todos os seus amigos e família clicando em 

# Recursos utilizados

| Nome           | Tipo?        | Função  |
| :------------- |:-------------| :-----  |
| aponte para \[*nome*\]  |  Movimento |  apontar o ator para a posição do mouse ou para outro ator  |
| diga \[*texto*\] por (*número*) segundos | Aparência | mostrar um texto por alguns segundos | 
| envie \[*mensagem*\] para todos | Eventos | . | 
| espere (*número*) segundos | Controle | . | 
| mova (*número*) passos  | Movimento |   . |
| mude \[*nome*\] para (*número*) | Variáveis | . | 
| mude para a fantasia \[*nome*\] | Aparência | . |
| próxima fantasia  |  Aparência |   . |
| quando clicar na bandeira | Eventos | definir que blocos devem ser executados quando o jogo começa | 
| quando receber \[*mensagem*\] | Eventos | . | 
| se \<*condicao*\> | Controle | definir que blocos devem ser executados quando a condição é verdadeira | 
| sempre      | Controle |  definir que blocos devem ser executados repetidamente enquanto o jogo estiver rodando |
| toque o tambor (*número*) por (*número*) batidas  | Música? |   . |
| vá para \[*nome*\]  | Movimento | mover ator para uma posição aleatória ou para a posição do mouse |

# Pendências

* repropor o uso de caixa alta no texto da cartilha
* repropor o uso do estilo destaque em laranja
* atualizar os ícones para os usados na versão 3 
* destacar novos blocos nos algoritmos
* atualizar os itens de menu citados para os usados na versão 3
* apresentar os 5 passos no início
* repropor o nome e a descrição de cada um dos passos
* repropor as tarefas de cada passo
* repropor os algoritmos 
* repropor as tarefas de teste
* reconsiderar o uso simultaneo das palestras jogo e projeto
* tentar o uso de um passo por página 
* citar os cartões no roteiro
* referência exemplos prontos no final?
* discutir com os facilitadores o comparativo entre a elaboração de uma receita culinária e a elaboração de um programa de computador
* apresentar a solução completa, do palco e dos atores
* repropor estrutura atual com 9 folhas: 1 capa, 1 contra capa e 7 páginas 