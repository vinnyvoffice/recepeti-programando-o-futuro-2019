# Cartilha 1

Criando o jogo Félix e Herbert com Scratch

## Começando 

Nós vamos criar um jogo de pega-pega, com o gato Félix e o rato Herbert. Você controla o Herbert com o mouse e tenta fugir do Félix. Quanto mais tempo você ficar sem ser pego, mais pontos você ganhará. Cuidado, se for pego, perderá pontos!

`#paracegover imagem do jogo pronto`

* [ ] 1. Antes de começar confira se o idioma do programa é o `Português Brasileiro`: clique no ícone do globo ao lado do logotipo do Scratch.

``` #paracegover imagem do idioma em Português Brasileiro```

Dica: Você pode acompanhar o seu progresso marcando o quadro ao lado de cada item.

## Passo 1: Félix persegue o ponteiro do mouse

Vamos fazer com que o Félix persiga o ponteiro do mouse.

* [ ] 1. Crie um novo projeto. `Arquivo > Novo` 
* [ ] 2. Escolha um cenário para o jogo. Nossa sugestão é o `Interior > Hall`
* [ ] 3. Altere o nome do ator para Félix.
* [ ] 4. Altere o estilo de rotação do ator para `Esquerda-Direita`.
* [ ] 5. Crie um conjunto de blocos no roteiro do Félix para perseguir o mouse.

```
```

* [ ] Teste o projeto: clique na bandeira verde.
    * [ ] O Félix está seguindo o ponteiro do mouse?
    * [ ] Ele mexe as pernas enquanto se movimenta?
    * [ ] A velocidade que ele se move é correta? 
* [ ] Salve o projeto: `Arquivo > Salvar agora` ou `Arquivo > Baixar para o seu computador`    

``` #paracegover imagem do passo 1 do jogo```

## Passo 2: Félix persegue o Herbert

Vamos fazer com que o Félix persiga o Herbert ao invés do ponteiro do mouse. 

* [ ] 1. Crie um novo ator. `Selecione um Ator > Animais > Mouse1` 
* [ ] 2. Altere o nome do ator para Herbert. 
* [ ] 3. Altere o estilo de rotação do ator para `Esquerda-Direita`.
* [ ] 4. Reduza pela metade o tamanho do rato. `Campo tamanho`
* [ ] 5. Crie um conjunto de blocos no roteiro do Herbert para que ele acompanhe o ponteiro do mouse

```
```

* [ ] Teste o projeto: clique na bandeira verde.
    * [ ] O Herbert acompanha o ponteiro do mouse?
    * [ ] O Félix está perseguindo o Herbert?
* [ ] Salve o projeto: `Arquivo > Salvar agora` ou `Arquivo > Baixar para o seu computador`    

``` #paracegover imagem do passo 2 do jogo```


## Passo 3: Félix fala quando toca no Herbert

Vamos fazer com que o Félix diga "Te Peguei!" quando ele pegar o Herbert. 

* [ ] 1. Adicione ao roteiro do Félix os blocos que fazem ele falar quando ele toca no Herbert.

```
```
* [ ] Teste o projeto: clique na bandeira verde.
    * [ ] O Félix diz algo quando pega o Herbert?
* [ ] Salve o projeto: `Arquivo > Salvar agora` ou `Arquivo > Baixar para o seu computador`    

``` #paracegover imagem do passo 3 do jogo```

## Passo 4: Herbert vira fantasma quando for pego

Vamos fazer o Herbert virar um fantasma quando for pego pelo Félix.

* [ ] 1. Altere o roteiro do Félix para enviar uma mensagem quando ele pegar o Herbert. Não é mais necessário falar.

```
```

* [ ] 2. Adicione uma fantasia de fantasma ao Herbert. `Fantasias > Escolher Fantasia > Imaginários > Ghost-c` 

* [ ] 3. Reduza pela metade o tamanho do fantasma. `Campo tamanho`
* [ ] 4. Altere o nome da fantasia do rato para `solto` e a do fantasma para `pego'.
* [ ] 5. Adicione blocos ao roteiro do Herbert para que ele vire fantasma quando recebe a mensagem enviada pelo Félix.

```
```

* [ ] Teste o projeto: clique na bandeira verde.
    * [ ] O Herbert vira fantasma quando o Félix é pego?
    * [ ] Qual som toca quando o Félix pega o Herbert?
    * [ ] O Félix fica parado tempo suficiente para o Herbert fugir?
* [ ] Salve o projeto: `Arquivo > Salvar agora` ou `Arquivo > Baixar para o seu computador`    

``` #paracegover imagem do passo 4 do jogo```


## Passo 5: Jogador perde pontos quando Félix pega Herbert

Vamos criar um placar para saber se estamos indo bem. O placar começa do zero e aumenta um ponto a cada segundo. Quando o Félix pegar o Herbert, o jogador deve perder cem pontos.

* [ ] 1. Crie uma variável chamada **placar**. Palco, `Variáveis`, botão `Criar uma Variável`. 
* [ ] 2. Crie 2 conjuntos de blocos no palco para contar os pontos.

```
```

* [ ] Teste o projeto: clique na bandeira verde.
    * [ ] O placar aumenta um ponto a cada segundo?
    * [ ] O placar diminui 100 pontos quando o Herbert é pego?
    * [ ] O que acontece quando o Herbert é pego antes do placar chegar a 100?
    * [ ] O placar volta a zero quando você começa um novo jogo? 
* [ ] Salve o projeto: `Arquivo > Salvar agora` ou `Arquivo > Baixar para o seu computador`    

``` #paracegover imagem do passo 5 do jogo```


## Desafios 

Para tornar o jogo mais divertido, tente fazer as seguintes alterações

* [ ] 1. Altere o movimento do Félix para ele ande mais rápido
* [ ] 2. Altere a pontuação para que não aconteça dos pontos serem negativos
* [ ] 3. Altere para que o Félix aponte para o Herbert e o Herbert vá para uma posição aleatória a cada 2 segundos

## Parabéns

Você acaba de criar o seu primeiro jogo! Não esqueça que você pode compartilhar seu jogo com todos os seus amigos e família clicando em `Compartilhar`

## Blocos Utilizados

* Variáveis > adicione **número** a variável
* Movimento > aponte para **ator**
* Movimento > aponte para **ponteiro do mouse**
* Controle > espere **número** segundos
* Movimento > mova **número** passos 
* Variáveis > mude **variável** para **número**
* Aparência > próxima fantasia
* Eventos > quando **bandeira verde** for clicado
* Eventos > quando eu receber **mensagem**
* Música > toque instrumento **nome** por **número** segundos
* Eventos > transmita **mensagem**
* Controle > se **condição** então 
* Controle > sempre
* Movimento > vá para **ator**
* Movimento > vá para **ponteiro do mouse**
