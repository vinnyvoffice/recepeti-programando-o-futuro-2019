# Capa

> * identidade visual equivalente ao da apostila de referência 
> * inclusive logotipos e imagens

Cartilha 1

Material adaptado de:
* code club 

Execução Técnica
* FEPESE
* recepeti

Coordenação e financiamento
* UFSC
* Ministério da Ciência, Tecnologia, Inovações e Comunicações
* Governo Federal

# Página de Apresentação e Preparação

> * identidade visual equivalente ao da apostila de referência 

Criando o jogo Félix e Herbert com o Scratch 

## Apresentando

Nós vamos criar um jogo de pega-pega, com o gato Félix e o rato Herbert. Você vai controlar o Herbert com o mouse do computador e vai tentar fugir do Félix. Quanto mais tempo o Herbert ficar sem ser pego, mais pontos você vai ganhar. Cuidado pois pontos são perdidos se o Herbert for pego!
Enquanto o rato Herbert consegue escapar do gato Félix, o jogador ganha pontos. Toda vez que o gato pega o rato, o jogador perde pontos. 

`colocar aqui imagem do jogo finalizado sem desafios - cartilha1-pagina1-figura1`

`colocar aqui imagem do jogo finalizado com desafios - cartilha1-pagina1-figura2`

> Fonte das imagens: <https://scratch.mit.edu/projects/313333712/editor/> e <https://scratch.mit.edu/projects/313333316/editor/> 

## Preparando

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

* Você já deve ter feito o Jogo da Coleta. Ele utiliza blocos de todas as 8 seções básicas.
* Você já deve ter feito o Jogo do Animal de Estimação Virtual. Atenção especial ao uso de variáveis, à transmissão e ao recebimento de mensagens e ao uso de número aleatório.
* Confirme que o idioma do Scratch é **Português Brasileiro** `colocar aqui imagem do ícone de troca de idioma - cartilha1-pagina1-figura3`.
* Você pode acompanhar seu progresso marcando o quadrinho ao lado de cada item.

# Passo 1: Félix persegue o ponteiro do mouse

> * identidade visual equivalente ao da apostila de referência 

## Orientações

* Crie um novo projeto
* Defina o nome do gato como **Félix**
* Defina os roteiros

## Roteiros 

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

### Para o gato Félix 

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Félix deve mover alguns passos
        Félix deve apontar pro ponteiro do mouse
        Félix deve mostrar a próxima fantasia
        Félix deve tocar o instrumento afoxé por algumas batidas
```

### Blocos sugeridos

> a imagem de cada um dos blocos abaixo pode ser obtida no seguinte editor <https://scratch.mit.edu/projects/313330727/editor/>. 
> colocar uma imagem por bloco
> notar que os blocos estão sequenciados em ordem alfabética
> usar a seguinte referência `cartilha1-passo1-blocos`

aponte para **ponteiro do mouse** 

quando ícone da bandeira verde for clicado             

mova **10** passos        

próxima fantasia

sempre

toque instrumento **Afoxé** por **0.01** batidas 

## Teste seu jogo

Clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

 * Félix deve seguir o mouse do computador
 * Félix deve manter certa distância do mouse do computador
 * Félix deve mexer as pernas enquanto se movimenta
 * Félix deve fazer um som enquanto se movimenta

# Passo 2: Félix persegue o Herbert

> * identidade visual equivalente ao da apostila de referência 

## Orientações

* Selecione um novo ator: o rato!
* Defina o nome do rato como **Herbert**
* Defina os roteiros 

## Roteiros 

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o rato Herbert 

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Herbert deve ir em direção ao ponteiro do mouse
        Herbert deve apontar pro Félix
```

## Blocos Sugeridos

> a imagem de cada um dos blocos abaixo pode ser obtida no seguinte editor <https://scratch.mit.edu/projects/313331557/editor>. 

aponte para **Félix **                          

quando ícone da bandeira verde for clicado             

sempre

vá para **ponteiro do mouse**                     

## Teste seu jogo 

Clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

* Herbert deve se movimentar com o mouse do computador
* Herbert deve se virar para o Félix

# Passo 3: Félix fala quando pega o Herbert 

> * identidade visual equivalente ao da apostila de referência 

## Orientações

* Identique que o Félix tocou no Herbert
* Faça o Félix falar quando toca no Herbert
* Defina os roteiros

## Roteiros 

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o gato Félix 

```
quando o jogo iniciar 
    as ações abaixo sempre devem ser repetidas
        manter os blocos do passo 1
        se o Herbert for tocado pelo Félix
        	Félix deve dizer “Te peguei” durante 1 segundo
```

## Blocos Sugeridos

> a imagem de cada um dos blocos abaixo pode ser obtida no seguinte editor <https://scratch.mit.edu/projects/313332549/editor>. 

diga **Te peguei!** por **1** segundos         

quando ícone da bandeira verde for clicado      

se *condição* então                              

sempre

tocando em **Herbert** ?                          

## Teste seu jogo 

Clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

* Félix deve falar algo quando toca no Herbert 

# Passo 4: Herbert troca de fantasia quando é pego pelo Félix

> * identidade visual equivalente ao da apostila de referência 

## Orientações

* Avise o Herbert e o cenário padrão quando o Félix pegar o rato.
* Define o nome da fantasia do Herbert com as patas para frente como **rato-frente**
* Define o nome da fantasia do Herbert com as patas para trás como **rato-atras**
* Crie uma fantasia pro Herbert que indique que ele foi pego. Dica: duplique uma das fantasias e desenhe um X sobre ela.
* Define o nome da nova fantasia do Herbert como **rato-pego**
* Defina os roteiros

## Roteiros 

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

### Para o gato Félix 

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        se o Herbert for tocado pelo Félix
        	Félix deve transmitir a mensagem “peguei”  
	        Félix deve trocar o instrumento conga por algumas batidas
```

### Para o rato Herbert 

```
quando receber a mensagem “peguei”
	Herbert deve mudar sua fantasia para pego
	Herbert deve esperar um pouco
	Herbert deve voltar para sua fantasia inicial
```

## Blocos Sugeridos

> a imagem de cada um dos blocos abaixo pode ser obtida no seguinte editor <https://scratch.mit.edu/projects/313333251/editor>. 

espere **0.5** seg                                 

mude para a fantasia **rato-frente**             

mude para a fantasia **rato-pego**               

quando eu receber **peguei**                     

se *condição* então                              

sempre 

tocando em **Herbert** ?                          

toque instrumento **Conga** por **0.25** batidas  

transmita **peguei**                              

## Teste seu jogo 

Clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

* Herbert deve mudar a fantasia quando for pego pelo Félix

## Passo 5: Herbert ganha pontos enquanto escapa e perde quando é pego pelo Félix

> * identidade visual equivalente ao da apostila de referência 

## Orientações

* Crie uma variável chamada **pontos**
* Clique no cenário padrão (*Palco*)
* Defina os roteiros 

## Roteiros 

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

### Para o cenário padrão 
```
quando o jogo iniciar 
    Cenário padrão define o valor 0 para a variável pontos
    as ações abaixo sempre devem ser repetidas
	    Cenário padrão aumenta o valor da variável pontos em 1 a cada um segundo
```

```
quando receber a mensagem peguei
	Cenário padrão diminui o valor da variável pontos em 100
```

## Blocos Sugeridos

> a imagem de cada um dos blocos abaixo pode ser obtida no seguinte editor <https://scratch.mit.edu/projects/313333316/editor>. 

adicione **1** a **pontos**                      

adicione **-100** a **pontos**                   

espere **1** seg                                

mude **pontos** para **0**                       

quando eu receber **peguei**                     

quando ícone da bandeira verde for clicado      

## Teste seu jogo 

Clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar

* Jogo deve aumentar os pontos enquanto o gato não pega o rato
* Jogo deve diminuar os pontos toda vez que o gato pega o rato

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

# Desafios 

> * identidade visual equivalente ao da apostila de referência 

## Orientações

> usar a caixa de verificação (*checklist*) do lado esquerdo de cada item abaixo e não do lado direito como está na cartilha de referência

Resolva um desafio e clique no ícone  `colocar aqui ícone da bandeira verde` para iniciar o jogo e testar

* Escolha um cenário para o jogo
* Tente diminuir o tamanho do Félix e do Herbert para tornar o jogo mais difícil
* Tente diminuir a velocidade do Félix para tornar o jogo mais fácil
* Tente alterar o estilo de direção do Félix para que ele não fique de cabeça para baixo
* Tente animar o movimento do Herbert

# Todos os blocos sugeridos

Parabéns!!!

Você concluiu uma etapa importante do aprendizagem de programação. Marque abaixo os blocos utilizados em cada etapa. 

> * a imagem de cada um dos blocos abaixo pode ser obtida nos passos anteriores 
> * permitir ao aluno marcar qual bloco foi utilizado em cada passo
> * utilizar mesmo quadrinho de marcação usado nas seções anteriores

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    adicione **1** a **pontos**    

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    adicione **-100** a **pontos**                  

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    aponte para **Félix **                          

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    aponte para **ponteiro do mouse**               

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    aponte para a direção **90**                     

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    diga **Te peguei!** por **1** segundos         

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    espere **0.1** seg                                

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    espere **0.5** seg                                 

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    espere **1** seg                                

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    mova **10** passos                               

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    mude **pontos** para **0**                       

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    mude para a fantasia **rato-atras**               

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    mude para a fantasia **rato-frente**             

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    mude para a fantasia **rato-pego**               

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    próxima fantasia                                 

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    quando eu receber **peguei**                     

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    quando ícone da bandeira verde for clicado      

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    se *condição* então                              

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    sempre                                           

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    tocando em **Herbert** ?                          

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    transmita **peguei**                              

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    toque instrumento **Afoxé** por **0.01** batidas  

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    toque instrumento **Conga** por **0.25** batidas  

[ ] passo 1 [ ] passo 2 [ ] passo 3 [ ] passo 4 [ ] passo 5 [ ] desafios    vá para **ponteiro do mouse**   

E compartilhe seu jogo com seus amigos e família clicando em `colocar aqui a imagem do botão Compartilhar do editor`.


# Contra Capa

> * identidade visual equivalente ao da apostila de referência 
> * inclusive logotipos e imagens

Cartilha 1

Material adaptado de:
* code club 

Execução Técnica
* FEPESE
* recepeti

Coordenação e financiamento
* UFSC
* Ministério da Ciência, Tecnologia, Inovações e Comunicações
* Governo Federal