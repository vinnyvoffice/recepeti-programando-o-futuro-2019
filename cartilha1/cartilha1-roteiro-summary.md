# Objetivo

Enquanto o rato Herbert consegue escapar do gato Félix, o jogador ganha pontos. Toda vez que o gato pega o rato, o jogador perde pontos. 

# Blocos por Elemento

| Bloco / Elemento                                  |     Félix      |    Herbert   |  cenário padrão  | 
| ---                                               | :------------: | :----------: | :--------------: | 
| adicione **1** a **pontos**                       |                |              |         x        | 
| adicione **-100** a **pontos**                    |                |              |         x        | 
| aponte para **Félix **                            |                |       x      |                  | 
| aponte para **ponteiro do mouse**                 |       x        |              |                  | 
| aponte para a direção **90**                      |       x        |              |                  | 
| diga **Te peguei!** por **1** segundos            |       x        |              |                  | 
| espere **0.1** seg                                |                |       x      |                  | 
| espere **0.5** seg                                |                |       x      |                  | 
| espere **1** seg                                  |                |              |         x        | 
| mova **10** passos                                |       x        |              |                  | 
| mude **pontos** para **0**                        |                |              |         x        | 
| mude para a fantasia **rato-atras**               |                |       x      |                  | 
| mude para a fantasia **rato-frente**              |                |       x      |                  | 
| mude para a fantasia **rato-pego**                |                |       x      |                  | 
| próxima fantasia                                  |       x        |              |                  | 
| quando eu receber **peguei**                      |                |       x      |         x        | 
| quando ícone da bandeira verde for clicado        |       x        |       x      |         x        | 
| se *condição* então                               |       x        |              |                  | 
| sempre                                            |       x        |       x      |         x        | 
| tocando em **Herbert**                            |       x        |              |                  | 
| transmita **peguei**                              |       x        |              |                  | 
| toque instrumento **Afoxé** por **0.01** batidas  |       x        |              |                  | 
| toque instrumento **Conga** por **0.25** batidas  |       x        |              |                  | 
| vá para **ponteiro do mouse**                     |                |       x      |                  | 

# Estrutura da cartilha

Nós vamos criar um jogo de pega-pega, com o gato Félix e o rato Herbert. Você vai controlar o Herbert com o mouse do computador e vai tentar fugir do Félix. Quanto mais tempo você ficar sem ser pego, mais pontos você vai ganhar. Cuidado pois se for pego, perderá pontos.

## Antes de fazer esse jogo

* utilize os cartões do jogo da coleta
* utilize os cartões do animal de estimação virtual
* faça o guia sobre listas
* confirme que o idioma é **Português Brasileiro** 

## Passo 1: Félix persegue o ponteiro do mouse

https://scratch.mit.edu/projects/313330727/

### Orientações

* Crie um novo projeto
* Clique no gato 
* Defina o nome do gato como **Félix**
* Defina os roteiros

### Roteiros 

#### Para o gato Félix 

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Félix deve mover alguns passos
        Félix deve apontar pro ponteiro do mouse
        Félix deve mostrar a próxima fantasia
        Félix deve tocar o instrumento afoxé por algumas batidas
```

### Blocos sugeridos

aponte para **ponteiro do mouse** 
quando ícone da bandeira verde for clicado             
mova **10** passos        
próxima fantasia
sempre
toque instrumento **Afoxé** por **0.01** batidas 

### Teste seu jogo

## Passo 2: Félix persegue o Herbert

https://scratch.mit.edu/projects/313331557/

### Orientações

* Selecione o rato como novo ator (*Mouse1*)
* Define o nome do rato como **Herbert**
* Defina os roteiros 

### Roteiros

#### Para o rato Herbert 

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Herbert deve ir em direção ao ponteiro do mouse
        Herbert deve apontar pro Félix
```

### Blocos sugeridos

quando ícone da bandeira verde for clicado             
sempre
vá para **ponteiro do mouse**                     
aponte para **Félix **                          

### Teste seu jogo                  


## Passo 3: Félix fala quando pega o Herbert 

https://scratch.mit.edu/projects/313332549/

### Orientações

* Um ator transmite uma mensagem para cenários e outros atores para que façam algo que ele não pode fazer
* Defina os roteiros 

### Roteiros

#### Para o gato Félix 

```
quando o jogo iniciar para o Félix
    as ações abaixo sempre devem ser repetidas
        se o Herbert for tocado pelo Félix
        	Félix deve dizer “Te peguei” durante 1 segundo
```

### Blocos sugeridos

quando ícone da bandeira verde for clicado      
sempre
se *condição* então                              
tocando em **Herbert** ?                          
diga **Te peguei!** por **1** segundos         

### Teste seu jogo

## Passo 4: Herbert troca de fantasia quando é pego pelo Félix

https://scratch.mit.edu/projects/313333251/

### Orientações

* Um ator ou cenário pode receber uma mensagem e reage executando blocos
* Crie uma fantasia pro Herbert que indique que ele foi pego. Dica: duplique uma das fantasias e desenhe um X sobre ela.
* Define o nome da fantasia com as patas para frente como **rato-frente**
* Define o nome da fantasia com as patas para trás como **rato-atras**
* Define o nome da nova fantasia como **rato-pego**
* Defina os roteiros

### Roteiros

#### Para o gato Félix 

```
quando o jogo iniciar para o Félix
    as ações abaixo sempre devem ser repetidas
        se o Herbert for tocado pelo Félix
        	Félix deve transmitir a mensagem “peguei”  
	        Félix deve trocar o instrumento conga por algumas batidas
```

#### Para o rato Herbert 

```
quando Herbert receber a mensagem “peguei”
	Herbert deve mudar sua fantasia para pego
	Herbert deve esperar um pouco
	Herbert deve voltar para sua fantasia inicial
```

### Blocos sugeridos

espere **0.5** seg                                 
mude para a fantasia **rato-frente**             
mude para a fantasia **rato-pego**               
quando eu receber **peguei**                     
se *condição* então                              
sempre 
tocando em **Herbert** ?                          
transmita **peguei**                              
toque instrumento **Conga** por **0.25** batidas  

### Teste seu jogo

## Passo 5: Herbert ganha pontos enquanto escapa e perde quando é pego pelo Félix

https://scratch.mit.edu/projects/313333316/

### Orientações

* Crie uma variável chamada **pontos**
* Clique no cenário padrão (*Palco*)
* Defina os roteiros 

### Roteiros

#### Para o cenário padrão 
```
quando o jogo iniciar 
    Cenário padrão define o valor 0 para a variável pontos
    as ações abaixo sempre devem ser repetidas
	    Cenário padrão aumenta o valor da variável pontos em 1 a cada um segundo
```

```
quando receber a mensagem peguei
	Cenário padrão diminui o valor da variável pontos em 100
```

### Blocos sugeridos

adicione **1** a **pontos**                      
adicione **-100** a **pontos**                   
espere **1** seg                                
mude **pontos** para **0**                       
quando eu receber **peguei**                     
quando ícone da bandeira verde for clicado      

### Teste seu jogo

## Desafios

https://scratch.mit.edu/projects/313333712/editor/

* Escolha um cenário 
* Tente diminuir o tamanho do Félix e do Herbert para tornar o jogo mais difícil
* Tente diminuir a velocidade do Félix para tornar o jogo mais fácil
* Tente alterar o estilo de direção do Félix para que ele não fique de cabeça para baixo
* Tente animar movimento do Herbert

## Todos os blocos

adicione **1** a **pontos**                      
adicione **-100** a **pontos**                   
aponte para **Félix **                          
aponte para **ponteiro do mouse**               
aponte para a direção **90**                     
diga **Te peguei!** por **1** segundos         
espere **0.1** seg                                
espere **0.5** seg                                 
espere **1** seg                                
mova **10** passos                               
mude **pontos** para **0**                       
mude para a fantasia **rato-atras**               
mude para a fantasia **rato-frente**             
mude para a fantasia **rato-pego**               
próxima fantasia                                 
quando eu receber **peguei**                     
quando ícone da bandeira verde for clicado      
se *condição* então                              
sempre                                           
tocando em **Herbert** ?                          
transmita **peguei**                              
toque instrumento **Afoxé** por **0.01** batidas  
toque instrumento **Conga** por **0.25** batidas  
vá para **ponteiro do mouse**   

# Referências

## Guia (workshop)

### Overview
* imagine
* create
* share 

### Get Ready for the Workshop
* Preview the tutorial
* Print the Activity Cards
* Make sure participants have Scratch accounts
* Set up computers or laptops
* Set up a computer with projector or large monitor

### Image
* 10 minutes
* First, gather as a group to introduce the theme and spark ideas. 
* Begin by gathering the participants to introduce the theme and spark ideas for projects. 
    * warm-up activity
    * provide ideas and inspiration 
    * demonstrate the first steps
        * choose a letter
        * make it do something
        * add a sound
        * choose a backdroup
### Create
* 40 minutes
* Next, help participants as they animate their names, working at their own pace
* Support participants as they create interactive name projects. 
    * start with prompts
    * provide resources: online tutorial or printed activity cards
    * suggest ideas for starting 
        * choose a letter
        * make it change color
        * add a sound
        * add a backdrop 
    * more thinks to try
        * draw a letter
        * make it spin
        * make it glide
        * change size
        * add more letters and motion
    * support collaboration
        * when someone gets stuck, connect then to another participant who can help.
        * see a cool idea? ask the creator to share with others.
    * encourage experimenting
        * help participants feel comfortable trying different combinations of blocks and seeing what happens.
### Share
* 10 minutes
* At the end of the session, gather together to share and reflect. 
* Have participants share their project with their neighbors.
    * ask questions they can discuss
* What's Next?
    * other names
    * start with an image
    * acrostics

## Cartões 

### Apresentação do jogo 

* 4 imagens de soluções diversas 
* objetivo do jogo

### Lista de cartões

Use estes cartões na seguinte ordem
* 1 Apresentação
* 2 Anime seu animal 
* 3 Hora de comer
* 4 Hora de beber
* 5 O que ele vai dizer?
* 6 Hora de brincar
* 7 Está com fome? 

#### Para cada cartão
* nome
* objetivo do cartão
* imagens
* prepare-se
* adicione este código
* teste
* dicas
