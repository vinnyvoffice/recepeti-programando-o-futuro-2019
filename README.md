# Referências

* https://scratch.mit.edu/starter-projects 
* Pong Starter - https://scratch.mit.edu/projects/10128515/
* Maze Starter - https://scratch.mit.edu/projects/10128431/
* Dress Up Tera - https://scratch.mit.edu/projects/11656680/
* Save the Mini-Figs - https://scratch.mit.edu/projects/10123832/
* Crechers - https://scratch.mit.edu/projects/65950146/
* MasterMind - https://scratch.mit.edu/projects/246251/
* Battleship - https://scratch.mit.edu/projects/69459/
* Mine Sweeper - https://scratch.mit.edu/projects/16856031/
* Create Your Own Princess: A Dress Up Game - https://scratch.mit.edu/projects/169982638/
* Race car Simulator - https://scratch.mit.edu/projects/38041040/
* Enduro - https://scratch.mit.edu/projects/106782641/
* Enduro underwork - https://scratch.mit.edu/projects/146568/
* River Raid - https://scratch.mit.edu/projects/20428294/
* Atari Seaquest - https://scratch.mit.edu/projects/190361/
* Mortal Kombat - https://scratch.mit.edu/projects/537840/
* Reciclagem - https://scratch.mit.edu/projects/47094024/
* Arraste e Solte - https://scratch.mit.edu/projects/13431347/
* Genius - https://scratch.mit.edu/projects/764628/
* Atari Pong - https://scratch.mit.edu/projects/161104/
* Atari Missile Command - https://scratch.mit.edu/projects/57527748/
* Atari 2014 - https://scratch.mit.edu/projects/25261534/
* Atari ADVENTURE - https://scratch.mit.edu/projects/222785370/
* Hangman - https://scratch.mit.edu/projects/172369086/
* Monopoly - https://scratch.mit.edu/projects/23006490/
* Board Games / Scratch Studio - https://scratch.mit.edu/studios/5656006/
* Avoid! Run Away!! - https://scratch.mit.edu/projects/299089371/
* Bubble Jump - https://scratch.mit.edu/projects/306407568/
