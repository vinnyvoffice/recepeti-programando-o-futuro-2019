# Cartilha 2

Criando o jogo Caça aos Dragões com o Scratch.

## Apresentando 

Vamos criar um jogo de caça aos dragões. Você deve capturar os dragões que aparecem na tela. Cada vez que um dragão for capturado, você ganha pontos. O objetivo é ganhar o máximo de pontos em 30 segundos! Cuidado com a bruxa pois ela pode tirar alguns segundos do seu tempo.

`#paracegover imagem do jogo Caça aos Dragões pronto`

## Preparando 

* [ ] Você já deve ter feito a animação Anime um Nome. Ela utiliza vários blocos de movimento e de aparência.
* [ ] Você já deve ter feito a animação Faça Música. Ela utiliza número aleatório, troca de fantasia e espera.
* [ ] Você já deve ter feito a animação Vamos Dançar. Ela utiliza repetição, fala de ator, som, animação, carimbo e movimentos.
* [ ] Você deve copiar o arquivo de imagem da bruxa (**blue-witch.svg**)
* [ ] Confirme que o idioma do Scratch é Português Brasileiro 

Dica: Você pode registrar o seu progresso marcando o quadro ao lado de cada item.

## Passo 1: Dragão voa no cenário

Vamos fazer com que um dragão voe sobre o cenário.

### Orientações

* [ ] 1. Crie um novo projeto.
* [ ] 2. Escolha um cenário para o jogo. Nossa sugestão é o `Exterior > Woods`.
* [ ] 3. Adicione um novo ator. Nossa sugestão é o `Imaginários > Dragon`. 
* [ ] 4. Reduza o tamanho do dragão para 25%.
* [ ] 5. Crie uma variável chamada **velocidade** para ser usada apenas por esse ator. Ela poderá ser usada para aumentar ou diminuir a velocidade do dragão.
* [ ] 5. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o dragão

```
quando o jogo iniciar
    Dragão deve definir o valor 5 para a velocidade
    Dragão deve ter seu estilo de rotação definido como esquerda-direita
    as ações abaixo sempre devem ser repetidas
        Dragão deve mover alguns passos
        Dragão deve voltar se tocar na borda
        Dragão deve trocar de fantasia
        Dragão deve espera 0.1 segundos
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

    * [ ] O dragão deve se mover de um lado para o outro no cenário.
    * [ ] O dragão deve virar o corpo corretamente quando alcança a borda do cenário.
    * [ ] O dragão deve soltar fogo quando se move.

``` #paracegover imagem do passo 1 do jogo pronto```

## Passo 2: Dragão aparece e desaparece

Vamos fazer o dragão aparecer e desaparecer aleatoriamente. Isso deve acontecer repetidamente até o jogo acabar.

### Orientações

* [ ] 1. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o dragão

```
quando o jogo iniciar
    as ações abaixo sempre devem ser repetidas
        Dragão deve desaparecer
        Dragão deve esperar entre 1 e 3 segundos
        Dragão deve reaparecer
        Dragão deve esperar entre 1 e 3 segundos
        Dragão deve deslizar por um segundo até uma posição aleatória
``` 

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] O dragão deve aparecer e desaparecer repetidamente
* [ ] O dragão deve se deslocar para posições diferentes do cenário

``` #paracegover imagem do passo 2 do jogo pronto```


## Passo 3: Jogador captura o dragão

Vamos fazer o dragão sumir e um som ser tocado quando o jogador clica sobre esse ator.

### Orientações

* [ ] 1. Escolha um som para ser tocado quando o dragão for clicado. Nossa sugestão é o `Pop`. 
* [ ] 2. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o dragão

```
    quando este ator for clicado
        Dragão deve desaparecer
        Dragão deve tocar um som por completo
        Dragão deve esperar um segundo
``` 


### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] O dragão deve desaparecer quando é clicado.
* [ ] O dragão deve tocar um som até o fim quando é clicado.


``` #paracegover imagem do passo 3 do jogo pronto```


## Passo 4: Jogador ganha pontos quando pega um dragão

Vamos marcar pontos cada vez que clicamos no dragão. Também vamos inserir um limite de tempo para a partida. 

### Orientações

* [ ] 1. Crie uma variável chamada **pontos** para ser usada por todos os atores. Ela armazenará os pontos do jogador.
* [ ] 2. Crie uma variável chamada **cronometro** para ser usada por todos os atores. Ela armazenará o tempo do jogo na unidade de tempo segundos.
* [ ] 3. Esconda a variável velocidade.
* [ ] 4. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para o dragão

```
    quando este ator for clicado
        Dragão deve somar o valor 1 à pontos
        Dragão deve desaparecer
        Dragão deve tocar um som por completo
        Dragão deve esperar um segundo
``` 

#### Para o cenário 


```
    quando o jogo iniciar
        Cenário deve definir o valor 0 para pontos

    quando o jogo iniciar
        Cenário deve definir o valor 30 para cronometro
        as ações abaixo devem ser repetidas até que o cronometro seja menor ou igual a 0
            Cenário deve esperar um segundo
            Cenário deve diminuir o valor 1 de cronometro
        Cenário deve para todo os roteiros
``` 

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] Pontos devem iniciar em 0
* [ ] Jogador deve ganhar 1 ponto toda vez que captura o dragão
* [ ] Cronometro deve iniciar em 30
* [ ] Cronometro deve diminuir a cada segundo
* [ ] A velocidade do dragão não deve ser apresentada

``` #paracegover imagem do passo 4 do jogo pronto```

## Passo 5: Jogador perde pontos quando pega a bruxa

### Orientações

* [ ] 1. Adicione um novo ator. Nossa sugestão é importar a fantasia `blue-witch.svg` indicada pelo educador. 
* [ ] 2. Escolha um som para ser tocado quando a bruxa for clicada. Nossa sugestão é o `Wobble`. 
* [ ] 2. Defina os roteiros.

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### Para a bruxa

```
    quando o jogo iniciar
        as ações abaixo sempre devem ser repetidas
            A bruxa deve somar o valor 10 à coordenada x de posicionamento horizontal
            se a bruxa tocar na borda do cenário então 
                A bruxa deve ir para o valor -240 da coordenada x e para o valor -10 da coordenada y
                a bruxa deve somar o valor 100 à coordenada x de posicionamento horizontal

    quando este ator for clicado
        A bruxa deve diminuir o valor 5 do cronometro
        A bruxa deve tocar um som
        A bruxa deve avisar que o jogador perdeu alguns segundos

```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] A bruxa deve voar da esquerda para a direita
* [ ] O jogador deve perde 5 segundos quando clica na bruxa
* [ ] Um som deve tocar quando o jogador clica na bruxa
* [ ] A bruxa deve avisar que o jogador perdeu alguns segundos

``` #paracegover imagem do passo 5 do jogo pronto```

## Desafios

### Orientações

Resolva um desafio e clique no ícone `bandeira verde` para iniciar o jogo e testar. 

* [ ] 1. Crie mais 2 dragões duplicando o primeiro e modificando as cores e os nomes para fiquem diferentes. Nossa sugestão: faça um dragão de cada cor e de o nome da cor escolhida como nome de cada um dos dragões.
* [ ] 2. Posicione os dragões em lugares diferentes do palco ao iniciar o jogo.
* [ ] 3. Altere a velocidade de cada dragão. Mostre a velocidade na tela para garantir que cada dragão está com uma velocidade diferente. Depois esconda a variável para não atrapalhar o jogo. Nossa sugestão: use as velocidades 5, 10 e 15.
* [ ] 4. Forneça  mais pontos para dragões capturados mais rápidos. Nossa sugestão: 3 pontos para o dragão de velocidade 15, 2 pontos para o de velocidade 10 e 1 ponto para o de velocidade 5.


### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] 3 dragões devem voar pelo cenário
* [ ] Cada dragão deve voar com velocidade diferente
* [ ] Cada dragão deve pontuar diferente dependendo da velocidade
* [ ] As variáveis de velocidade não devem ser apresentadas

``` #paracegover imagem do passo 1 do jogo pronto```

## Todos os blocos sugeridos

Abaixo marque os blocos utilizados em cada etapa.

...

## Parabéns

Dessa vez você aprendeu a criar novos atores a partir de atores já existentes, importar fantasias para dentro do editor além de fazer um jogo que exige uma ótima pontaria para capturar os dragões sem esbarrar na bruxa. 

Não esqueça que você pode compartilhar seu jogo com todos os seus amigos e família clicando em `Compartilhar`.

## Notas do aluno

## Parabéns

Não esqueça que você pode compartilhar seu jogo com todos os seus amigos e família clicando em `Compartilhar`. 

# Resumo

## Etapas

* Passo 1: Dragão voa no cenário
* Passo 2: Dragão aparece e desaparece
* Passo 3: Jogador pega o dragão
* Passo 4: Jogador ganha pontos quando pega um dragão
* Passo 5: Jogador perde pontos quando pega a bruxa
* Desafios

## Elementos

### Palco

* 1 cenário: woods

    quando ícone verde for clicado 
        mude (pontos) para (0)
        cronometro em segundos (30)
    
    defina (cronometro em segundos (number))
        mude (cronometro) para (number)
        repita até que (cronometro < 0 ou cronometro = 0)
            espere (1) seg
            adicione (-1) a (cronometro)
        pare (todos)

### Atores

#### Ator 1: green-dragon

    quando ícone verde for clicado
        mude (velocidade) para (15)

    quando ícone verde for clicado
        sempre
            mova (velocidade) passos
            próxima fantasia
            espere (0.01) seg

    quando ícone verde for clicado
        sempre
            esconda
            espere (número aleatório entre (1) e (3)) seg
            mostre 
            espere (número aleatório entre (1) e (3)) seg
            se tocar na borda, volte
            deslize por (1) segs. até (posição aleatória)

    quando este ator for clicado
        adicione (3) a (pontos)
        toque o som (Pop) até o fim
        esconda
        espere (1) seg

#### Ator 2: magenta-dragon

    quando ícone verde for clicado
        mude (velocidade) para (10)
        
    quando ícone verde for clicado
        sempre
            mova (velocidade) passos
            próxima fantasia
            espere (0.01) seg

    quando ícone verde for clicado
        sempre
            esconda
            espere (número aleatório entre (1) e (3)) seg
            mostre 
            espere (número aleatório entre (1) e (3)) seg
            se tocar na borda, volte
            deslize por (1) segs. até (posição aleatória)

    quando este ator for clicado
        adicione (3) a (pontos)
        toque o som (Pop) até o fim
        esconda
        espere (1) seg


#### Ator 3: pink-dragon

    quando ícone verde for clicado
        mude (velocidade) para (5)
        
    quando ícone verde for clicado
        sempre
            mova (velocidade) passos
            próxima fantasia
            espere (0.01) seg

    quando ícone verde for clicado
        sempre
            esconda
            espere (número aleatório entre (1) e (3)) seg
            mostre 
            espere (número aleatório entre (1) e (3)) seg
            se tocar na borda, volte
            deslize por (1) segs. até (posição aleatória)

    quando este ator for clicado
        adicione (3) a (pontos)
        toque o som (Pop) até o fim
        esconda
        espere (1) seg


#### Ator 4: blue-witch

    quando ícone verde for clicado
        sempre
            adicione (10) a x
            se (tocgando em (borda)?) então
                vá para x: (-240) y: (-10)
                adicione (100) a x

    quando este ator for clicado
        toque o som (Wobble)
        diga (Perdeu 5 segundos ;) ) por (2) segundos
        adicione (-5) a (cronometro)

### Variáveis 

* Variável 1: cronometro
* Variável 2: pontos
* Variável 3: velocidade

## Blocos Utilizados

* Variáveis > adicione **número** a variável
* Movimento > aponte para **ator**
* Movimento > aponte para **ponteiro do mouse**
* Controle > espere **número** segundos
* Movimento > mova **número** passos 
* Variáveis > mude **variável** para **número**
* Aparência > próxima fantasia
* Aparência > esconda
* Aparência > mostre
* Operadores > número aleatório entre **número1** e **número2**
* Controle > pare todos
* Eventos > quando **bandeira verde** for clicado
* Eventos > quando eu receber **mensagem**
* Eventos > quando este ator for clicado
* Música > toque instrumento **nome** por **número** segundos
* Som > toque o som **nome**
* Eventos > transmita **mensagem**
* Controle > repita até **condição**  
* Controle > se **condição** então 
* Movimento > se tocar na borda, volte
* Controle > sempre
* Movimento > vá para **ator**
* Movimento > vá para **ponteiro do mouse**
