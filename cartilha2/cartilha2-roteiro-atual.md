# Cartilha 2

Criando o jogo Caça aos Dragões com o Scratch.

## Apresentando 

Vamos criar um jogo de caça aos dragões. Você deve capturar os dragões que aparecem na tela. Cada vez que um dragão for capturado, você ganha pontos. O objetivo é ganhar o máximo de pontos em 30 segundos! Cuidado com a bruxa pois ela pode tirar segundos do seu tempo.

`#paracegover imagem do jogo Caça aos Dragões pronto`

## Preparando 

* [ ] Você já deve ter feito o jogo ... Ele utiliza ... 
* [ ] Você já deve ter feito o jogo ... Ele utiliza ... 
* [ ] Você já deve ter feito o jogo ... Ele utiliza ... 
* [ ] Você deve copiar o arquivo de imagem da bruxa disponível em ...
* [ ] Confirme que o idioma do Scratch é Português Brasileiro 

Dica: Você pode registrar o seu progresso marcando o quadro ao lado de cada item.

## Passo 1: Dragão voa no cenário

Vamos fazer com que um dragão voe sobre a floresta.

### Orientações

* 1 [ ] Crie um novo projeto Scratch (Arquivo - Novo)
* 2 [ ] Clique em Palco, na área embaixo à esquerda da tela
* 3 [ ] Sem seguida, vá até a aba Panos de fundo, Importar fundo da biblioteca `ícone correspondente` e escolha o fundo naturez/woods.
* 4 [ ] Adicione um novo ator, clicando em importar ator da biblioteca `ícone correspondente` (na área em baixo, à esquerda) e selecione imaginários/witch1
* 5 [ ] Clique em váriaveis e crie uma variável chamada "velocidade". Atenção: selecione a opção para este ator apenas `imagem da opção selecionada`
Você poderá usar esta variável mais tarde para mudar a velocidade da bruxa durante o jogo. Para que a bluxa comece a se mexer quando o jogo iniciar, crie estes blocos: 
* [ ] 1. Crie um novo projeto. `Arquivo > Novo` 
* [ ] 2. Escolha um cenário para o jogo. Nossa sugestão é o `Exterior > Woods`
* [ ] 3. Adicione um novo ator. `Selecione um Ator > Imaginários > Dragon` 
* [ ] 4. Crie uma variável chamada **velocidade** para o dragão. A variável poderá ser usada mais tarde para mudar a velocidade do dragão durante o jogo.
* [ ] 5. Adicione os blocos abaixo ao roteiro do dragão para que ele se movimente. 



```
quando clicar na Bandeira
    mude [velocidade v] para (5)
    sempre
        mova (**velocidade**) passos
    fim
```

* [ ] 6 Para não ficar trancada a bruxa deve dar meia volta quando tocar na borda do palco. Abaixo do comando mova, adicione um comando `imagem do bloco se tocar na borda, volte`

```
quando clicar na Bandeira
    mude [velocidade v] para (5)
    sempre
        mova (velocidade) passos
        se tocar na borda, volte
    fim
```
* [ ] 7. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

    * [ ] O dragão fica trancado no canto da tela? Altere o roteiro do dragão para que volte quando tocar na borda.
```
```
    * [ ] O dragão fica de cabeça para abaixo ao voltar? Altere o estilo de rotação para `Esquerda-Direita`
    * [ ] O dragão se move de um lado para o outro  da tela?
    * [ ] O dragão mostra a mesma fantasia quando se movimenta? Experimente trocar a fantasia enquanto a cada vez que se movimenta. O dragão vai mover as patas e soltar fogo.

``` #paracegover imagem do passo 1 do jogo pronto```




## Passo 2: Dragão aparece e desaparece

Vamos fazer o dragão aparecer e desaparecer aleatoriamente. Isso deve acontecer repetidamente até o jogo acabar.

### Orientações

```
quando clicar na Bandeira
    sempre
        desapareça
        espere (sorteie número entre (2) e (5)) segundos
        apareça
        espere (sorteie número entre (3) e (5)) segundos
    fim
``` 

* [ ] 7. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] O dragão aparece e desaparece repetidamente? 
* [ ] Experimente demorar mais para aparecer ou desaparecer 
* [ ] Experimente demorar menos para aparecer ou desaparecer 
* [ ] Experimente aumentar a velocidade do dragão cada vez que ele reaparece


``` #paracegover imagem do passo 2 do jogo pronto```


## Passo 3: Jogador pega o dragão

Vamos fazer o dragão sumir e um som ser tocado quando o jogador clica sobre o ator com o mouse.

### Orientações

```
    quando este ator for clicado
        desapareça
        toque o som [Fairydust v]
``` 
* [ ] 1. Escolha um som para ser tocado quando o dragão for clicado. `Sons > Selecionar um Som > Pop`
* [ ] 2. Adicione os blocos abaixo ao roteiro do dragão para que ele desapareça e toque o som.
* [ ] 3. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar

* [ ] O dragão desaparece e toca o som quando é clicado?


``` #paracegover imagem do passo 3 do jogo pronto```


## Passo 4: Jogador ganha pontos quando pega um dragão

Vamos marcar pontos cada vez que clicamos no dragão. Também vamos inserir um limite de tempo para a partida. 

### Orientações

```
    quando este ator for clicado
        desapareça
        toque o som [Fairydust v]
        mude [placar v] por (1)
``` 

```
    quando clicar na Bandeira
        mude [contagem regressiva v] para (30)
        mude [placar v] para (0)
        repita até <[contagem regressiva] - [0]>
            espere (1) segundos
            mude [contagem regressiva v] para (-1)
        fim 
        pare tudo    
``` 

* [ ] 1. Crie uma variável chamada **placar** para todos os atores. E modifique o roteiro do dragão para adicionar 1 a esta variável toda vez que ele é clicado.
* [ ] 2. Crie uma variável chamada **cronometro** para todos os atores. E adicione os blocos abaixo ao roteiro do palco para que o cronometro comece de 30 e o placar de 0. E que cronometro reduza a cada segund até zerar. 
* [ ] 7. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar


``` #paracegover imagem do passo 4 do jogo pronto```

## Passo 5: Jogador perde pontos quando pega a bruxa

### Orientações

* [ ] 7. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar


``` #paracegover imagem do passo 5 do jogo pronto```

## Desafios

### Orientações

```
``` 

Para tornar o jogo mais divertido, tente fazer as seguintes alterações

* [ ] 1. Crie mais 2 dragões duplicando o primeiro e modifique-os para fiquem diferentes. Dica: altere o tamanho ou a cor. 
* [ ] 2. Posicione os dragões em lugares diferentes do palco .
* [ ] 3. Altere a velocidade de cada dragão. Mostre a velocidade na tela para garantir que cada dragão está com uma velocidade diferente. Depois esconda a variável para não atrapalhar o jogo.
* [ ] 4. Forneça mais pontos para dragões mais rápidos. Por exemplo, adicione 1 ponto pro mais lento e 10 pontos pro mais rápido
* [ ] 7. Defina os roteiros

### Roteiros

Para programar o que cada personagem deve fazer, escolha os blocos necessários na seção de blocos sugeridos.

#### 1 
```
```

#### 2 
```
```

### Blocos Sugeridos

...

### Teste seu jogo

Clique no ícone `bandeira verde` para iniciar


``` #paracegover imagem do passo 1 do jogo pronto```


# Resumo

## Etapas

* Passo 1: Dragão voa no cenário
* Passo 2: Dragão aparece e desaparece
* Passo 3: Jogador pega o dragão
* Passo 4: Jogador ganha pontos quando pega um dragão
* Passo 5: Jogador perde pontos quando pega a bruxa
* Desafios

## Elementos

### Palco

* 2 cenários: ?

### Atores

#### Ator 1: ?

### Variáveis 

* Variável 1: ?

## Blocos Utilizados

* Seção > bloco

## Recursos utilizados

| Nome           | Tipo?        | Função  |
| :------------- |:-------------| :-----  |
| apareça | Aparência | . | 
| desapareça | Aparência | . | 
| espere (*número*) segundos | Controle | . | 
| mova (*número*) passos  | Movimento |   . |
| mude \[*nome*\] para (*número*) | Variáveis | . | 
| quando clicar na bandeira | Eventos | definir que blocos devem ser executados quando o jogo começa | 
|     quando este ator for clicado | Eventos | . | 
| repita até <*condição*> | Controle | . | 
| se tocar na borda, volte | Movimento | . | 
| sempre  | Controle |  definir que blocos devem ser executados repetidamente enquanto o jogo estiver rodando |
| sorteie número entre (*número*) e (*número*) | Operadores | . | 
| toque o som \[*nome*\] | Som | . | 