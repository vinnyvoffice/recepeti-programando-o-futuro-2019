# Guias

## Jogo da Coleta

scratch.mit.edu/catch

Crie um jogo no qual você pega coisas que caem do céu.

7 cartões

### Vá para o topo

mude y para **180**
quando ícone da bandeira verde for clicado    
vá para **ponteiro do mouse**
vá para **posição aleatória**

### Faça cair 

quando ícone da bandeira verde for clicado    
sempre
adicione **-5** a y 
se **posição y** < **parte inferior do palco** então
vá para **posição aleatória**
mude y para **180**
mude y para **0** 

### Mova o coletor

quando clicar em bandeira verde
sempre
adicione -5 a y
se tecla seta para a direita pressionada? então
adicione 10 a x
se tecla seta para a esquerda pressionada então
adicione -10 a x

### Colete!

quando clicar em bandeira verde
sempre
se tocando em Bowl então
toque o som pop
vá para posição aleatória
mude y para 180

### Marque pontos

quando clicar em bandeira verde
mude pontos para 0 
sempre
se tocando em Bowl? então
toque o som pop
adicione a pontos 1
vá para posição aleatória
mude y para 180

### Pontos extras 

quando clicar em bandeira verde
mude pontos para 0 
sempre
se tocando em Bowl? então
toque o som pop
adicione a pontos 2
vá para posição aleatória
mude y para 180

### Você ganhou!

quando clicar em bandeira verde
esconda
espere até pontos = 5
mostre
para todos

## Jogo da moda

scratch.mit.edu/fashion

Vista um personagem com vários modelos e estilos de roupas. 

7 cartões 

### Escolha um personagem

### Brique com as cores

### Altere o estilo

### Mude o pano de fundo

### Exiba suas roupas

### Deslize

### Encaixe

## Jogo de Esconde-esconde

scratch.mit.edu/hide 

Faça um jogo de esconde-esconde com personagens que aparecem e desaparecem.

6 cartões

### Esconda

### Clique e diga

### Tempo surpresa

### Aleatoriedade

### Marque pontos

### Esconderijo

## Vamos dançar !

Faça uma dança animada, com música e passos de dança. 

scratch.mit.edu/dance

10 cartões 

### Passos de dança

### Repetição

### Reproduza a música

### Revezamento

### Posição inicial

### Efeito de sombra

### Deixe um rastro

### Efeito de cores

### Movimentos 

### Dança interativa

## Faça algo voar

Escolha qualquer ator e faça-o voar!

scratch.mit.edu/fly

7 cartões

### Escolha um personagem

### Comece a voar

### Altere o cenário

### Mova usando teclas

### Nuvens flutuantes

### Corações voadores

### Marque pontos 

## Criar Música

Escolha instrumentos, coloque sons e pressione as teclas para tocar música

scratch.mit.edu/music

9 cartões

### Toque o tambor

### Crie um ritmo

### Anime o tambor

### Crie uma melodia

### Toque um acorde

### Música surpresa

### Sons de beatbox

### Gravar som

### Tocar uma música

## Anime seu nome

Anime as letras do seu nome, suas iniciais ou palavra favorita. 

scratch.mid.edu/name

7 cartões

### Clique colorido

### Gire

### Reproduza um som

### Letra dançante

### Altere o tamanho

### Pressione uma tecla

### Deslize

## Jogo Pong

Crie uma versão do jogo da bola saltitante com efeitos sonoros, pontos, e outros efeitos.

scratch.mit.edu/pong

6 cartões

### Crie movimento

### Mova a raquete

### Quicando na raquete

### Fim de jogo

### Marque pontos

### Ganhe o jogo

## Corrida olímpica

Crie um jogo em que os personagens disputam corrida.

scratch.mit.edu/racegame

7 cartões

### Inicie a corrida

### Em suas marcas

### Linha de chegada 

### Escolha um competidor

### Adicione um som

### Anime a corrida

### Competição 

## Crie uma história

Escolha os personagens, crie diálogos e dê vida a sua história.

scratch.mit.edu/story

9 cartões 

### Inicie uma história

### Mostre um personagem

### Adicione um diálogo

### Altere o cenário

### Deslize

### Aparição em cena

### Interatividade

### Adicione sua voz

### Adicione um botão

## Animal de estimação virtual

Crie um animal de estimação virtual interativ que come, bebe e brinca.

scratch.mit.edu/pet

7 cartões

### Apresentação

quando ícone da bandeira verde for clicado    
vá para x: **0** y: **160**
diga **Meu nome é Kiki!** por **1** segundos

### Anime seu animal

quando este ator for clicado
toque o some **chee chee**
repita **4** vezes
mude para a fantasia **monkey2-b**
espere **0.2** seg
mude para a fantasia **monkey2-a**
espere **0.2** seg

### Hora de comer


quando este ator for clicado
vá para a frente
vá para x: **-190** y: **-120** 
envie comida a todos

quando receber **comida**
deslize por 1 seg até x: -190 y: -100
toque o som **chomp** 
espere **0.5** seg
deslize por 1 seg até x: -60 y: 80

### Hora de beber

quando este ator for clicado
vá para a frente
vá para x: -80 y: -120
envie bebida a todos
espere 1 seg
mude para a fantasia glass water-b
toque o som water drop
espere 1 seg
mude para a fantasia glas water-a

quando receber bebida 
deslize por 1 seg até x: -80 y: -100
espere 1 seg
deslize por 1 seg até x: -60 y: 100

### O que ele vai dizer ?

quando este ator for clicado
mude opção para número aleatório entre 1 e 3
se opção = 1 então
diga "Eu gosto de banana!" por 2 segundos
se opção = 2 então 
diga "Isso faz cócegas! por 2 segundos
se opção = 3 então 
diga "Vamos brincar!" por 2 segundos

### Hora de brincar

quando este ator for clicado 
vá para x: 130 y: -120
envie brincar a todos 

quando receber brincar
vá para a frente 
deslize por 1 seg até x: 120 y: -40
repita 4 vezes
adicione 20 a y
espere 0.3 seg
adicione -20 a y
espere 0.3 seg
deslize por 1 seg até x: -60 y: 100

### Está com fome? 

quando clicar em bandeira verde
mude fome para 0
sempre
adicione a fome 1
espere 5 seg

quando receber comida
adicione a fome -1

# Cartões


## Cartilhas

adicione **1** a **pontos**                      
adicione **-100** a **pontos**                   
aponte para **Félix **                           
aponte para **ponteiro do mouse**                
aponte para a direção **90**                    
diga **Te peguei!** por **1** segundos           
espere **0.1** seg                               
espere **0.5** seg                              
espere **1** seg                                
mova **10** passos                             
mude **pontos** para **0**                      
mude para a fantasia **rato-atras**               
mude para a fantasia **rato-frente**            
mude para a fantasia **rato-pego**              
próxima fantasia                                
quando eu receber **peguei**                    
quando ícone da bandeira verde for clicado    
se *condição* então                              
sempre                                          
tocando em **Herbert**                          
transmita **peguei**                             
toque instrumento **Afoxé** por **0.01** batidas 
toque instrumento **Conga** por **0.25** batidas  
vá para **ponteiro do mouse**                    


